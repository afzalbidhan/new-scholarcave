<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    //return $request->user();

});


Route::post('/login','AuthController@login')->name('apiLogin');

Route::post('/signup','AuthController@signup')->name('apiSignup');
// Route::post('signup',[AuthController::class, "signup"]);

Route::post('/become-teacher','\App\Http\Controllers\Api\Backend\TeacherController@become_teacher')->name('apiBecomeTeacher');

/*
|--------------------------------------------------------------------------
| Frontend Route
|--------------------------------------------------------------------------
*/


/*
|--------------------------------------------------------------------------
| backend Route
|--------------------------------------------------------------------------
*/


/*
| Admin User Routes
*/

Route::group(['prefix'=>'admin','as'=>'admin.'], function(){
    //Institute Crud Functionality (all)
    Route::resource('/institute', 'Api\Backend\Admin\InstituteController');
    //Course Crud Functionality (all)
    Route::resource('/course', 'Api\Backend\Admin\CourseController');
    //Teacher Approve Crud Functionality (update)
    Route::resource('/approve-teacher', 'Api\Backend\Admin\ApproveTeacherController');
     //User Crud Functionality (view all)
    Route::resource('/all-user', 'Api\Backend\Admin\UserController');
    //Profile information crud (All)
    Route::resource('/profile', 'Api\Backend\User\ProfileController');
    //Teacher Crud Functionality (view all)
    Route::resource('/all-teacher', 'Api\Backend\Admin\TeacherController');
    //CourseAssign Crud Functionality (all)
    Route::resource('/course-assign', 'Api\Backend\Admin\CourseAssignController');
});




/*
| General User Routes
*/

/*
| Teacher User Routes
*/
