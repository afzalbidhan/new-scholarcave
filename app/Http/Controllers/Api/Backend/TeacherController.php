<?php

namespace App\Http\Controllers\Api\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Teacher;
use App\User;
use Illuminate\Support\Facades\Auth;

class TeacherController extends Controller
{
    //
    function become_teacher(Request $request)
    {
        try {

            $user = User::where('email',$request->email)->first();
            //Teacher Creation
            $oldTeacher = Teacher::where('user_id',$user->id)->first();

            if($oldTeacher!=null)
            {
                return response()->json(['message' => 'You are alredy a Teacher']);
            }
            else{

                try {
                    $teacher = New Teacher;
                    $teacher->user_id = $user->id;
                    $teacher->name = $user->name;
                    $save = $teacher->save();
                    return response()->json(['message' => 'success']);
                } catch (\Throwable $ex) {
                    
                    return response()->json(['message' => 'failed']);
                }

            }

        } catch (\Throwable $ex) {
            return response()->json(['message' => 'failed']);
        }

    }
}
