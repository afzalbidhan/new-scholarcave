<?php

namespace App\Http\Controllers\Api\Backend\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Course;
use App\User;
use Illuminate\Support\Facades\Validator;

class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public $successStatus = 200;
    public function index()
    {
        //view all course data
        try {
            $data = Course::All();
            return response()->json(['message' => 'success','data' => $data], $this->successStatus);
        } catch (\Throwable $ex) {
            return response()->json(['message' => 'failed'], $this->successStatus);
        }
       

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //insert course

        
        $validator = Validator::make($request->all(), [
            'course_code' => 'max:191|unique:courses',
            'course_name' => 'max:191',
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }

        try {

           $user = User::where('id', $request->user_id)->first();
           $courseattributeNames = array(
                'ins_id'               => null,
                'course_code'          => $request->course_code,
                'course_name'          => $request->course_name,
                'created_by'           => $user->name,
                'updated_by'           => $user->name,
            );

            Course::create( $courseattributeNames);
            return response()->json(['message' => 'success'], $this->successStatus);

        } catch (\Throwable $ex) {
            return response()->json(['message' => 'failed'], $this->successStatus);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //show indivisual course
        try {
            $data = Course::where('id', $id)->first();
            return response()->json(['message' => 'success','data' => $data], $this->successStatus);
        } catch (\Throwable $ex) {
            return response()->json(['message' => 'failed'], $this->successStatus);
        }
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //edit view indivisual course
        try {
            $data = Course::where('id', $id)->first();
            return response()->json(['message' => 'success','data' => $data], $this->successStatus);
        } catch (\Throwable $th) {
            return response()->json(['message' => 'failed'], $this->successStatus);
        }
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //update course 
         $validator = Validator::make($request->all(), [
            'course_code' => 'max:191|unique:courses,course_code,'.$id,
            'course_name' => 'max:191',
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }

        try {

            $user = User::where('id', $request->user_id)->first();
            $courseattributeNames = array(
                 'ins_id'               => null,
                 'course_code'          => $request->course_code,
                 'course_name'          => $request->course_name,
                 'updated_by'           => $user->name,
             );
 
             Course::findOrFail($id)->update($courseattributeNames);
             return response()->json(['message' => 'success'], $this->successStatus);

        } catch (\Throwable $ex) {
            return response()->json(['message' => 'failed'], $this->successStatus);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //delete course
        try {
            $data = Course::where('id', $id)->first();
            $data->delete();
            return response()->json(['message' => 'success'], $this->successStatus);
        } catch (\Throwable $th) {
            return response()->json(['message' => 'failed'], $this->successStatus);
        }
        
    }
}
