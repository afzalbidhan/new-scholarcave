<?php

namespace App\Http\Controllers\Api\Backend\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\CourseAssign;
use App\User;
use Illuminate\Support\Facades\Validator;

class CourseAssignController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public $successStatus = 200;
    public function index()
    {
        //view all course data
        try {
            $data = CourseAssign::All();
            return response()->json(['message' => 'success','data' => $data], $this->successStatus);
        } catch (\Throwable $ex) {
            return response()->json(['message' => 'failed'], $this->successStatus);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $general_user_id = $request->general_user_id;
        $generalUserData = explode(',', $general_user_id);

        try {

            $user = User::where('id', $request->user_id)->first();
            for ($i=0; $i <count($generalUserData) ; $i++) { 
                $courseAssignAttributeNames = array(
                    'course_id'          => $request->course_id,
                    'general_user_id'    => $generalUserData[$i] ,
                    'created_by'         => $user->name,
                );
    
                CourseAssign::create( $courseAssignAttributeNames);
            }

            return response()->json(['message' => 'success'], $this->successStatus);

        } catch (\Throwable $ex) {
            return response()->json(['message' => 'failed'], $this->successStatus);
        } 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        try {

            $user = User::where('id', $request->user_id)->first();
            $courseattributeNames = array(
                 'ins_id'               => null,
                 'course_code'          => $request->course_code,
                 'course_name'          => $request->course_name,
                 'updated_by'           => $user->name,
             );
 
             Course::findOrFail($id)->update($courseattributeNames);
             return response()->json(['message' => 'success'], $this->successStatus);

        } catch (\Throwable $ex) {
            return response()->json(['message' => 'failed'], $this->successStatus);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        //delete course
        try {
            $data = CourseAssign::where('id', $id)->first();
            $data->delete();
            return response()->json(['message' => 'success'], $this->successStatus);
        } catch (\Throwable $th) {
            return response()->json(['message' => 'failed'], $this->successStatus);
        }
    }
}
