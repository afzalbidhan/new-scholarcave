<?php

namespace App\Http\Controllers\Api\Backend\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Teacher;
use Illuminate\Support\Facades\Validator;

class ApproveTeacherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public $successStatus = 200;
    public function index()
    {
        //list of all teachers who aren't approve
        try {
            $data = Teacher::where('is_approved', '0')->get();
            return response()->json(['message' => 'success','data' => $data], $this->successStatus);
        } catch (\Throwable $ex) {
            return response()->json(['message' => 'failed'], $this->successStatus);
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //update teacher approval

        try {
            $data = Teacher::where('id', $id)->first();
            $data->is_approved = '1';
            $save = $data->save();
            return response()->json(['message' => 'success'], $this->successStatus);
        } catch (\Throwable $ex) {
            return response()->json(['message' => 'failed'], $this->successStatus);
        }

        /* $data = Teacher::where('id', $id)->first();
        $data->is_approved = '1';
        $save = $data->save();

        if($save){
            return response()->json(['message' => 'Data upadted successfully'], $this->successStatus);
        }else{
            return response()->json(['message' => 'failed'], $this->successStatus);
        } */

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
