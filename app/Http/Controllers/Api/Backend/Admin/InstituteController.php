<?php

namespace App\Http\Controllers\Api\Backend\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Institute;
use Illuminate\Support\Facades\Validator;


class InstituteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public $successStatus = 200;
    public function index()
    {
        //view all institue data
        try {
            $data = Institute::All();
            return response()->json(['message' => 'success','data' => $data], $this->successStatus);
        } catch (\Throwable $ex) {
            return response()->json(['message' => 'failed'], $this->successStatus);
        }
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //insert institue
        $validator = Validator::make($request->all(), [
            'institute_name' => 'max:191|unique:institutes',
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }

        try { 
            $instituteattributeNames = array(
                 'institute_name'       => $request->institute_name,
             ); 
            Institute::create($instituteattributeNames);
            return response()->json(['message' => 'success'], $this->successStatus);
        } catch (\Throwable $ex) {
            return response()->json(['message' => 'failed'], $this->successStatus);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //view indivisual institue
        try {
            $data = Institute::where('id', $id)->first();
            return response()->json(['message' => 'success','data' => $data], $this->successStatus);
        } catch (\Throwable $ex) {
            return response()->json(['message' => 'failed'], $this->successStatus);
        }
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //edit view indivisual institue
        try {
            $data = Institute::where('id', $id)->first();
            return response()->json(['message' => 'success','data' => $data], $this->successStatus);
        } catch (\Throwable $ex) {
            return response()->json(['message' => 'failed'], $this->successStatus);
        }
       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //update institute
        $validator = Validator::make($request->all(), [
            'institute_name' => 'max:191|unique:institutes,institute_name,'.$id,
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }

        try {
            $instituteattributeNames = array(
                'institute_name'       => $request->institute_name,
            ); 
            Institute::findOrFail($id)->update($instituteattributeNames);
            return response()->json(['message' => 'success'], $this->successStatus);

        } catch (\Throwable $ex) {
            return response()->json(['message' => 'failed'], $this->successStatus);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //delete institute
        try {
            $data = Institute::where('id', $id)->first();
            $data->delete();
            return response()->json(['message' => 'success'], $this->successStatus);
        } catch (\Throwable $ex) {
            return response()->json(['message' => 'failed'], $this->successStatus);
        }
    }
}
