<?php

namespace App\Http\Controllers\Api\Backend\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Model\AdminInfo;
use App\Model\Teacher;
use DB;
use Validator;
use App\Model\GeneralUser;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public $successStatus = 200;
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($email)
    {
        //
        try {
            $user = User::where('email', $email)->with('general_users')->with('teacher')->first();
            return response()->json(['message' => 'success', 'data' => $user], $this->successStatus);
        } catch (\Throwable $ex) {
            return response()->json(['message' => 'failed'], $this->successStatus);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        try {
            $user = User::where('id', $id)->with('general_users')->with('teacher')->first();
            return response()->json(['message' => 'success', 'data' => $user], $this->successStatus);
        } catch (\Throwable $th) {
            return response()->json(['message' => 'failed'], $this->successStatus);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   

        $request->name      =  $request->name == "null"? null : $request->name;
        $request->gender    =  $request->gender == "null"? null : $request->gender;
        $request->dob       =  $request->dob == "null"? null : $request->dob;
        $request->nid       =  $request->nid == "null"? null : $request->nid;
        $request->phone     =  $request->phone == "null"? null : $request->phone;
        $request->alt_email =  $request->alt_email == "null"? null : $request->alt_email;
        $request->address   =  $request->address == "null"? null : $request->address;        
        try {
            $user = User::where('id', $id)->first();
  
                //admin user update profile
                DB::beginTransaction();
                try {

                    $profileImage = $request->file('pro_pic');
                    $coverImage = $request->file('cover_pic');

                    $userAttributeNames = array(
                        'name'                => $request->name,
                        'email'               => $request->email,
                        'phone'               => $request->phone,
                    );

                    $attributeNames = array(
                        'name'                 => $request->name,
                        'gender'               => $request->gender,
                        'dob'                  => $request->dob,
                        'nid'                  => $request->nid,
                        'phone'                => $request->phone,
                        'alt_email'            => $request->alt_email,
                        'address'              => $request->address,
                    );

                                 
                    if($profileImage != ""){
                        $date = date('Y.m.d');
                        $profileImageName = str_replace('.', '', $date) . '1' . time();
                        $profileImageExt = strtolower($profileImage->getClientOriginalExtension());
                        $profileImageFullName = $profileImageName . '.' . $profileImageExt;
                        $profileImagePath = 'upload/backend/profile/';
                        $profileUrl = $profileImagePath . $profileImageFullName;
                        $moveProfile = $profileImage->move($profileImagePath, $profileImageFullName);
                        $attributeNames += ['pro_pic' => $profileUrl];

                    }
                    if($coverImage != ""){

                        $date = date('Y.m.d');
                        $coverImageName = str_replace('.', '', $date) . '2' . time();
                        $coverImageExt = strtolower($coverImage->getClientOriginalExtension());
                        $coverImageFullName = $coverImageName . '.' . $coverImageExt;
                        $coverImagePath = 'upload/backend/profile/';
                        $coverUrl = $coverImagePath . $coverImageFullName;
                        $moveCover = $coverImage->move($coverImagePath, $coverImageFullName);

                        $attributeNames += ['cover_pic' => $coverUrl];

                    }
                    
                    User::findOrFail($id)->update($userAttributeNames);
                    if($user->type == "1"){
                        AdminInfo::where('user_id', $id)->update($attributeNames);
                    }
                    else if($user->type == "2"){
                        GeneralUser::where('user_id', $id)->update($attributeNames);
                    }
                    else{
                        Teacher::where('user_id', $id)->update($attributeNames);
                    }
                    DB::commit();
                    return response()->json(['message' => 'success'], $this->successStatus);

                } catch (\Throwable $ex) {
                    DB::rollback();
                    return response()->json(['message' => $ex], $this->successStatus);
                }
        } catch (\Throwable $ex) {
            return response()->json(['message' => 'failed'], $this->successStatus);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
