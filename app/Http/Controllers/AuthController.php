<?php

namespace App\Http\Controllers;

use App\Model\GeneralUser;
use Illuminate\Http\Request;
use App\Model\UserType;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{

    public $successStatus = 200;
    public function login(Request $req)
    {
        // $credentials = $req->validate([
        //     'email'=>'required|email:rfc|max:50',
        //     'password'=>'required|min:3'
        // ]);

        // $user = User::where('email', $req['email'])
        // ->where('password', $req['password'])->first();
        // if($user){


        //     Auth::login($user);
        //     $accesstoken = Auth::user()->createToken('authToken')->accessToken;
        //     return response()->json(['user'=>Auth::user(),'accessToken'=>$accesstoken]);
        // }
        // else{
        //     return ['messge'=>'Could not find your account. Please try again'];
        // }



        if (Auth::attempt(['email' => request('email'), 'password' => request('password')])) {

            $user = Auth::user();

            $success['token'] = $user->createToken('MyApp')->accessToken;

            $success['id'] = $user->id;
            $success['name'] = $user->name;
            $type = UserType::where('id', $user->type)->get()->first();
            $success['type'] = $type->name;

            return response()->json(['message' => 'success', 'data' => $success], $this->successStatus);
        } else {
            return response()->json(['error' => 'Unauthorised'], 401);
        }
    }


    /////////////////////////// Sign up ////////////////////

    public function signup(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            'c_password' => 'required|same:password',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        //user creation
        $input = $request->all();


        $input['password'] = bcrypt($input['password']);
        $input['c_password'] = bcrypt($input['c_password']);

        $input['type'] = UserType::where('name', 'general_user')->first()->id;


        $user = User::create($input);
        //general user creation
        //dd($user->id);
        $general['name'] = $request->name;
        $general['user_id'] = $user->id;

        $general_user = GeneralUser::create($general);


        // auth login
        // Auth::login($user);
        // dd(Auth::user());


        //token creation
        $success['token'] =  $user->createToken('MyApp')->accessToken;
        $success['name'] =  $user->name;
        $success['id'] = $user->id;

        return response()->json(['message' => 'success', 'data' => $success], $this->successStatus);
    }
}
