<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CourseAssign extends Model
{
    //
    protected $fillable = [
        'course_id', 'general_user_id', 'created_by',
    ];
}
