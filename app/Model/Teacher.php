<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    //
    protected $fillable = [
        'user_id', 'name', 'gender','ins_id','dob', 'nid', 'phone','pro_pic', 'cover_pic',
        'alt_email','address', 'is_approved', 'approved_by',
    ];
}
