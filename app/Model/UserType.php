<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UserType extends Model
{
    //
    protected $fillable = [
        'name', 'email','password', 'phone','type',
    ];
}
