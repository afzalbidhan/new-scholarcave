<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Institute extends Model
{
    //
    protected $fillable = [
        'institute_name',
    ];
}
