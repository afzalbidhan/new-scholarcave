<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $fillable = [
        'ins_id', 'course_code', 'course_name','status','created_by','updated_by',
    ];
}
