<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class GeneralUser extends Model
{
    //
    protected $fillable = [
        'user_id', 'name', 'gender','ins_id','dob','nid','phone','pro_pic','cover_pic','alt_email','address'
    ];
    
}
