import React from 'react'

const BreadCrumb = ({title,openModal}) => {
    return (
        <div className="header bg-primary pb-6">
            <div className="container-fluid">
                <div className="header-body">
                    <div className="row align-items-center py-4">
                        <div className="col-lg-6 col-7">
                            <h6 className="h2 text-white d-inline-block mb-0">{title}</h6>
                            <nav aria-label="breadcrumb" className="d-none d-md-inline-block ml-md-4">
                               
                            </nav>
                        </div>
                        <div className="col-lg-6 col-5 text-right">
                            <a  className="btn btn-sm btn-neutral" onClick={openModal}>New</a>
                           
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default BreadCrumb
