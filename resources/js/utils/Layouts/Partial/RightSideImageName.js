import React from 'react'

const RightSideImageName = () => {
    return (
      // ml-auto ml-md-0 
        <ul className="navbar-nav align-items-center ">
                <li className="nav-item dropdown">
                  <a className="nav-link pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <div className="media align-items-center">
                      <span className="avatar avatar-sm rounded-circle">
                        <img alt="Image placeholder" src="../../argon/assets/img/theme/team-4.jpg" />
                      </span>
                      <div className="media-body  ml-2  d-none d-lg-block">
                        <span className="mb-0 text-sm  font-weight-bold">John Snow</span>
                      </div>
                    </div>
                  </a>
                  <div className="dropdown-menu  dropdown-menu-right ">
                    <div className="dropdown-header noti-title">
                      <h6 className="text-overflow m-0">Welcome!</h6>
                    </div>
                    <a href="" className="dropdown-item">
                      <i className="ni ni-single-02"></i>
                      <span>My profile</span>
                    </a>
                    <a href="" className="dropdown-item">
                      <i className="ni ni-settings-gear-65"></i>
                      <span>Settings</span>
                    </a>
                    <a href="" className="dropdown-item">
                      <i className="ni ni-calendar-grid-58"></i>
                      <span>Activity</span>
                    </a>
                    <a href="" className="dropdown-item">
                      <i className="ni ni-support-16"></i>
                      <span>Support</span>
                    </a>
                    <div className="dropdown-divider"></div>
                    <a href="" className="dropdown-item">
                      <i className="ni ni-user-run"></i>
                      <span>Logout</span>
                    </a>
                  </div>
                </li>
              </ul>
    )
}

export default RightSideImageName
