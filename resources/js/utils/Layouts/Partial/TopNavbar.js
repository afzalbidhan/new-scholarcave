import React from 'react'
import RightSideImageName from './RightSideImageName'
import RightSideMenu from './RightSideMenu'
import Search from './Search'

const TopNavbar = ({SearchComponetPlaceholder,logout}) => {
    return (
        <>

            <div className="navbar navbar-top navbar-expand navbar-dark bg-blue border-bottom">
                <div className="container-fluid">
                    <div className="collapse navbar-collapse" id="navbarSupportedContent">

                        <Search placeholder={SearchComponetPlaceholder} />

                        <RightSideMenu />

                        <RightSideImageName />

                    </div>
                </div>
            </div>
        </>
    )
}

export default TopNavbar
