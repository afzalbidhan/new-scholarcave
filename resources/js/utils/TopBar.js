import React, { Component } from "react";

import { connect } from "react-redux";
import { logoutWithJWT } from "../redux/actions/auth/loginActions";

const TopBar = (props) => {
    return (
        <header className="topbar-nav">
            <nav id="header-setting" className="navbar navbar-expand fixed-top">
                <ul className="navbar-nav mr-auto align-items-center">
                    <li className="nav-item">
                        <a
                            className="nav-link toggle-menu"
                            href=""
                        >
                            <i className="icon-menu menu-icon"></i>
                        </a>
                    </li>
                    <li className="nav-item">
                        <form className="search-bar">
                            <input
                                type="text"
                                className="form-control"
                                placeholder="Enter keywords"
                            />
                            <a href="">
                                <i className="icon-magnifier"></i>
                            </a>
                        </form>
                    </li>
                </ul>

                <ul className="navbar-nav align-items-center right-nav-link">
                    <li className="nav-item language">
                        <button className="btn btn-outline-info" onClick={props.logoutWithJWT}>Logout</button>
                    </li>
                    <li className="nav-item">
                        <a
                            className="nav-link dropdown-toggle dropdown-toggle-nocaret"
                            data-toggle="dropdown"
                            href="#"
                        >
                            <span className="user-profile">
                                <img
                                    src="assets/images/avatars/avatar-13.png"
                                    className="img-circle"
                                    alt="user avatar"
                                />
                            </span>
                        </a>
                        <ul className="dropdown-menu dropdown-menu-right">
                            <li className="dropdown-item user-details">
                                <a href="">
                                    <div className="media">
                                        <div className="avatar">
                                            <img
                                                className="align-self-start mr-3"
                                                src="assets/images/avatars/avatar-13.png"
                                                alt="user avatar"
                                            />
                                        </div>
                                        <div className="media-body">
                                            <h6 className="mt-2 user-title">
                                                Sarajhon Mccoy
                                            </h6>
                                            <p className="user-subtitle">
                                                mccoy@example.com
                                            </p>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li className="dropdown-divider"></li>
                            <li className="dropdown-item">
                                <i className="icon-envelope mr-2"></i> Inbox
                            </li>
                            <li className="dropdown-divider"></li>
                            <li className="dropdown-item">
                                <i className="icon-wallet mr-2"></i> Account
                            </li>
                            <li className="dropdown-divider"></li>
                            <li className="dropdown-item">
                                <i className="icon-settings mr-2"></i> Setting
                            </li>
                            <li className="dropdown-divider"></li>
                            <li className="dropdown-item">
                                <i className="icon-power mr-2" ></i> Logout
                            </li>
                        </ul>
                    </li>
                </ul>
            </nav>
        </header>
    );
};


const mapStateToProps = state => {
    return {
      values: state.auth.login
    }
  }

export default connect(mapStateToProps, { logoutWithJWT })(TopBar)

//export default TopBar;
