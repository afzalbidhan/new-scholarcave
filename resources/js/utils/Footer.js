import React, { Component } from "react";

const Footer = () => {
    return(
     <footer className="footer">
      <div className="container">
        <div className="text-center">
          Copyright © 2022 Scholarcave
        </div>
      </div>
    </footer>
    )
	
	
} 


export default Footer;