import React from "react";
import { BrowserRouter as Router, Route, Switch, Link ,Redirect} from "react-router-dom";
import Login from "../components/pages/Login";
import Signup from "../components/Signup";
import HomePage from "../components/pages/HomePage";
import BecomeTeacher from "../components/pages/BecomeTeacher";

const NoLayoutContent = () => {
    return (

           <>

                {/* <Route exact path="*" >
                    <Redirect to="/" />
                </Route>  */}
                <Route exact path="/login">
                    <Login />
                </Route>

                <Route exact path="/Signup" component ={Signup}/>


                <Route exact path="/">
                    <HomePage />
                </Route>


            </>


    );
};

export default NoLayoutContent;
