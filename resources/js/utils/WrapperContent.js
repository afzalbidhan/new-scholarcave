import React from "react";
import { BrowserRouter as Router, Route, Switch, Link ,Redirect  } from "react-router-dom";
import Navbar from "../utils/Navbar";
import About from "../components/pages/About";
import HomePage from "../components/pages/HomePage";
import Preregistration from "../components/pages/Preregistration";
import Booking from "../components/pages/Booking";
import AccountsAndInventory from "../components/pages/AccountsAndInventory";
import ActivationsAccountSide from "../components/pages/ActivationsAccountSide";
import CableAssignAccountSide from "../components/pages/CableAssignAccountSide";
import Login from "../components/pages/Login";
import PreregistrationOngoing from "../components/pages/PreregistrationOngoing";
const WrapperContent = () => {
    return (
        <div className="content-wrapper">
            <Switch>
                <Route path="/preregistration/preregOngoing">
                    <PreregistrationOngoing/>
                </Route>
                <Route path="/login">
                    <HomePage />
                </Route>
                <Route path="/accountsAndInventory/cableAssignAccountSide">
                    <CableAssignAccountSide />
                </Route>
                <Route path="/accountsAndInventory/activationAccountSide">
                    <ActivationsAccountSide />
                </Route>
                <Route path="/accountsAndInventory">
                    <AccountsAndInventory />
                </Route>
                <Route path="/booking">
                    <Booking />
                </Route>
                <Route path="/preregistration">
                    <Preregistration />
                </Route>
                <Route path="/about">
                    <About />
                </Route>
                <Route path="/">
                    <HomePage />
                </Route>
            </Switch> 

           
        </div>
    );
};

export default WrapperContent;
