import React, { Component, useEffect, useState } from "react";
import { BrowserRouter as Router, Route, Switch, Link } from "react-router-dom";
import { useHistory } from "react-router-dom";
// import Navbar from "./Navbar";
// import TopBar from "./TopBar";
import Footer from "./Layouts/Partial/Footer";
import AllRoutes from "./AllRoutes";
import { connect } from "react-redux";
import { loginStateUpdate } from "../redux/actions/auth/loginActions";
// import { logoutWithJWT } from "../redux/actions/auth/loginActions";
// import Search from "./Layouts/Partial/Search";
// import RightSideMenu from "./Layouts/Partial/RightSideMenu";
// import RightSideImageName from "./Layouts/Partial/RightSideImageName";
// import BreadCrumb from "./Layouts/Partial/BreadCrumb";
// import BreadCrumbRightSide from "./Layouts/Partial/BreadCrumbRightSide";
// import CardHeightFull from "./Layouts/Partial/CardHeightFull";
// import CardHeightHalf from "./Layouts/Partial/CardHeightHalf";
// import CardPageVisit from "./Layouts/Partial/CardPageVisit";
// import CardSocialTrafficing from "./Layouts/Partial/CardSocialTrafficing";
// import CardTotalOrders from "./Layouts/Partial/CardTotalOrders";
// import CardSales from "./Layouts/Partial/CardSales";
import TopNavbar from "./Layouts/Partial/TopNavbar";
import Body from "../components/pages/Dashboard";
import Sidebar from "./Layouts/Partial/Sidebar";
//import {Footer} from './Layouts/Partial/Footer'

const Wrapper = props => {
    const [status, setStatus] = useState(undefined);

    useEffect(() => {
        props.loginStateUpdate();
    }, []);

    const history = useHistory();

    const becomeTeacher = () => {
        location.href = "./become-teacher";
        //history.push('/become-teacher')
    };

    return (
        <>
            <Sidebar />

            <div className="main-content" id="panel">
                <TopNavbar SearchComponetPlaceholder="Search" />
                {/*
            <Body Footer={<Footer/>}/> */}

                <AllRoutes />
            </div>
        </>
    );
};


const mapStateToProps = state => {
    return {
        values: state.auth.login
    };
};

export default connect(mapStateToProps, { loginStateUpdate })(Wrapper);
