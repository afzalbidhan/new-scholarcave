import React,{useState,useEffect}from "react";
import { Route, Redirect } from "react-router-dom";


import {  useSelector } from 'react-redux';


export const ProtectedRoute =  ({
  component: Component,
  ...rest
}) => {

  const authChecker = useSelector(state => state.auth.login);
  // let token = authChecker.values;
  let token =  localStorage.getItem("token");
  return (
    <Route
      {...rest}
      render={props => {
        
        if (token) {
         
          
         
          return <Component {...props} />;
        } else {
          return (
            // <Redirect
            //   to={{
            //     pathname: "/login",
            //     state: {
            //       from: props.location
            //     }
            //   }}
            // />

           


            location.href='./login'
          );
        }
      }}
    />
  );
};




