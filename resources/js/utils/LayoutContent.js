import React from "react";
import { BrowserRouter as Router, Route, Switch, Link ,Redirect} from "react-router-dom";
import BecomeTeacher from "../components/pages/BecomeTeacher";
import Wrapper from "./Wrapper";
import AdminRoute from "./Route/AdminRoute";
import GeneralUserRoute from "./Route/GeneralUserRoute";

const LayoutContent = () => {
    const checkUser = localStorage.getItem('type')

    return (
        <>
            {checkUser==='admin' ? <AdminRoute/> : <GeneralUserRoute/>}

        </>
    )
}

export default LayoutContent
