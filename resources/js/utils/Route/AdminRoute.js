import React from "react";
import { BrowserRouter as Router, Route, Switch, Link ,Redirect} from "react-router-dom";
import AdDashboard from '../../components/pages/admin/AdDashboard';

const AdminRoute = () => {
    return (
        <>
            <Route exact path="/" component ={AdDashboard}/>
        </>
    )
}

export default AdminRoute
