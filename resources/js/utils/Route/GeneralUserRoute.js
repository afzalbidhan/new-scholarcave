import React from "react";
import { BrowserRouter as Router, Route, Switch, Link ,Redirect} from "react-router-dom";
import BecomeTeacher from "../../components/pages/BecomeTeacher";
import Wrapper from "../Wrapper";
import GeDashboard from '../../components/pages/general_user/GeDashboard';


const GeneralUserRoute = () => {
    return (
        <>
            <Route exact path="/" component ={GeDashboard}/>
            <Route exact path="/become-teacher" component ={BecomeTeacher}/>
        </>
    )
}


export default GeneralUserRoute
