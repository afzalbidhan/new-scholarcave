import React from "react";
import { BrowserRouter as Router, Route, Switch, Link } from "react-router-dom";
import Users from "../components/pages/Users";
import Teachers from "../components/pages/Teachers";
import Dashboard from "../components/pages/Dashboard";
import BecomeTeacher from "../components/pages/BecomeTeacher";
import Login from "../components/pages/Login";
import ApproveTeacher from "../components/pages/ApproveTeacher";
import Profile from "../components/pages/Profile";
import EditProfile from "../components/pages/EditProfile";
import IndividualUser from "../components/pages/IndividualUser";
import Course from "../components/pages/Course";

const AllRoutes = () => {
    return (
        <div  className="content-wrapper">
                <Route exact path="/" component={Dashboard}/>
                <Route exact path="/users" component={Users}/>
                <Route exact path="/become-teacher" component={BecomeTeacher}/>
                <Route exact path="/approve-teacher" component={ApproveTeacher}/>
                <Route exact path="/profile" component={Profile}/>

                <Route exact path="/profile/:id" component={IndividualUser}/>
                <Route exact path="/profile/:id/edit" component={EditProfile}/>
                <Route exact path="/teachers" component={Teachers}/>
                <Route exact path="/course" component={Course}/>
                {/* <Route exact path="*" component={() => "404 NOT FOUND"} />  */}

        </div>
    );
};

export default AllRoutes;
