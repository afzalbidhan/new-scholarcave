import axios from "axios";
import { reject } from "lodash";

export const loginWithJWT = user => {

    return dispatch =>  {
        const formData = {
            email: user.email,
            password: user.password
        };
         axios
            .post("/api/login", formData, {
                headers: {
                    "content-type": "application/json"
                }
            })
            .then(response => {
                var loggedInUser;

                if (response.data.message == 'success') {
                    if(response.data.data){

                     loggedInUser = response.data.data;
                        console.log(loggedInUser);
                      dispatch({
                          type: "LOGIN_WITH_JWT",
                          payload: { loggedInUser, loggedInWith: "jwt" }
                      });


                    }

                   history.push("/");
                }


            })
            .catch(err => {

             dispatch({
              type: "LOGIN_ERROR",
              payload: { error : err.response.status}
             });


            });
    };
};


export const loginStateUpdate = () =>{
    return dispatch => {
        let token = localStorage.getItem("token");
        dispatch({ type: "LOGIN_STATE_UPDATE", payload:token  });

    }
}



export const logoutWithJWT = () => {
    return dispatch => {

        localStorage.removeItem("token");
        dispatch({ type: "LOGOUT_WITH_JWT", payload: null });
        location.href= '/login';
          history.push("/pages/login")
    };
};

export const changeRole = role => {
    return dispatch => dispatch({ type: "CHANGE_ROLE", userRole: role });

};
