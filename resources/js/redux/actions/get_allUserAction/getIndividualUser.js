import axios from "axios";
import { readyException } from "jquery";

export const SHOW_INDIVIDUAL_USER=(id)=>{
    // alert(1)
    return dispatch => {
        axios
          .get(`/api/admin/all-user/${id}`)
          .then(response => {
            if(response.data.message == "success"){
                //console.log(response);
                //user = response.data


                let payload = response.data.data;

                //console.log(response)

                dispatch({
                    type: "SHOW_INDIVIDUAL_USER",
                    payload:payload
                })
            //  history.push("/")
            }
            else{
                //console.log(response.data);
                alert(response);
            }
          })
          .catch(err => console.log(err))
    }
}
