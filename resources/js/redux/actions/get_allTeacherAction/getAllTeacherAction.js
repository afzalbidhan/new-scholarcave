import axios from "axios";

export const GET_ALL_TEACHER=()=>{
    return dispatch => {
        axios
          .get("/api/admin/all-teacher")
          .then(response => {
            if(response.data.message == "success"){

                let payload = response.data.data;

                dispatch({
                    type: "GET_ALL_TEACHER",
                    payload:payload
                })
            }
            else{
                alert(response);
            }
          })
          .catch(err => console.log(err))
    }
}
