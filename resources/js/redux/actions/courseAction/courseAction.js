import axios from "axios";
import { readyException } from "jquery";

export const ADD_COURSE=(user)=>{

    const formData = {
        institute_id :user.institute_id,
        course_code :user.course_code,
        course_name :user.course_name,
        user_id: user.user_id,
    };

    return dispatch => {
        axios
          .post(`/api/admin/course`,formData,{
            headers: {
                "content-type": "application/json"
            }
        })
          .then(response => {
            if(response.data.message == "success"){
                GET_ALL_COURSE;
                return true;


            }
            else{
                //console.log(response.data);
                alert(response);
                return false
            }
          })
          .catch(err => console.log(err))
    }
}

export const DELETE_COURSE=(id)=>{


    return dispatch => {
        axios
          .delete(`/api/admin/course/${id}`)
          .then(response => {
            if(response.data.message == "success"){

                return true;


            }
            else{
                //console.log(response.data);
                alert(response);
                return false
            }
          })
          .catch(err => console.log(err))
    }
}



export const GET_ALL_COURSE=()=>{

    return dispatch => {
        axios
          .get(`/api/admin/course`)
          .then(response => {
            if(response.data.message == "success"){

                let payload = response.data.data;

                dispatch({
                    type: "GET_ALL_COURSE",
                    payload:payload
                })

            }
            else{
                alert(response);
            }
          })
          .catch(err => console.log(err))
    }
}

export const UPDATE_COURSE=(course)=>{

    return dispatch => {
        const fd = new FormData();
        fd.append('id',course.course_id );
        fd.append('user_id',course.user_id );
        fd.append('course_code',course.course_code );
        fd.append('course_name',course.course_name );
        fd.append('_method', 'PATCH');
        axios
          .post(`/api/admin/course/${course.course_id}`,fd,{
            headers: {
                'Accept' : 'application/json',
                //'Content-Type' : 'application/x-www-form-urlencoded',
                 'Content-Type' : 'multipart/form-data',
                }
            })
          .then(response => {
            if(response.data.message == "success"){

                let payload = response.data.data;
                alert('course updated');

            }
            else{
                alert('course not updated');
            }
          })
          .catch(err => console.log(err))
    }
}


export const GET_EDIT_COURSE = (id) =>{

   

    return dispatch => {
        axios
          .get(`/api/admin/course/`+id)
          .then(response => {
            console.log(response.data.message);
            if(response.data.message == "success"){

                let payload = response.data.data;
                payload.edited = 0;
                 
                dispatch({
                    type: "GET_EDIT_DATA",
                    payload:payload
                })

               

            }
            else{
                alert(response);
            }
          })
          .catch(err => console.log(err))
    }
  

}
