import axios from "axios";
import { readyException } from "jquery";

export const CURRENT_USER_PROFILE=(email)=>{

    return dispatch => {
        axios
          .get(`/api/admin/profile/${email}`)
          .then(response => {
            if(response.data.message == "success"){
                //console.log(response);
                //user = response.data


                let payload = response.data.data;

                //console.log(response)

                dispatch({
                    type: "CURRENT_USER_PROFILE",
                    payload:payload
                })
            //  history.push("/")
            }
            else{
                //console.log(response.data);
                alert(response);
            }
          })
          .catch(err => console.log(err))
    }
}

export const EDIT_USER_PROFILE=(id)=>{

    return dispatch => {
        axios
          .get(`/api/admin/profile/${id}/edit`)
          .then(response => {
            if(response.data.message == "success"){

                let payload = response.data.data;

                dispatch({
                    type: "EDIT_USER_PROFILE",
                    payload:payload
                })

            }
            else{
                alert(response);
            }
          })
          .catch(err => console.log(err))
    }
}

export const UPDATE_USER_PROFILE=(id,user)=>{

    /* return dispatch => {
        // const formData = {
        //     id:id,
        //     name : user.name,
        //     email : user.email,
        //     gender :user.gender,
        //     ins_id: user.ins_id,
        //     dob : user.dob,
        //     nid :user.nid,
        //     phone : user.phone,
        //     alt_email :user.alt_email,
        //     address : user.address,
        //     pro_pic : user.pro_pic.files[0],
        //     cover_pic : user.cover_pic.files[0],
        // }
        const fd = new FormData();
        // let pro =null
        // let cover =null;
        // if(user.pro_pic != null)
        // {
        //     pro =user.pro_pic.files[0]
        // }
        // if(user.cover_pic != null)
        // {
        //     cover =user.cover_pic.files[0]
        // }

        fd.append('id',id );
        fd.append('name',user.name );
        fd.append('email',user.email );
        fd.append('gender',user.gender );
        fd.append('ins_id',user.ins_id );
        fd.append('dob',user.dob );
        fd.append('nid',user.nid );
        fd.append('phone',user.phone );
        fd.append('alt_email',user.alt_email );
        fd.append('address',user.address );
        if(user.pro_pic != null)
        {
            fd.append('pro_pic',  user.pro_pic.files[0]);
        }
        if(user.cover_pic != null)
        {
            fd.append('cover_pic',   user.cover_pic.files[0]);
        }

        fd.append('_method', 'PATCH');

        //console.log(formData);
        axios
        //.patch(`/api/admin/profile/${id}`,formData,{
            .post(`/api/admin/profile/${id}`,fd,{
            headers: {
                'Accept' : 'application/json',
                //'Content-Type' : 'application/x-www-form-urlencoded',
                 'Content-Type' : 'multipart/form-data',
                }
            })
          .then(response => {
            if(response.data.message == "success"){
                alert("update profile done");
                console.log(response.data.message);
            }
            else{
                alert("update profile, something is wrong");
                console.log(response.data.message);
            }
          })
          .catch(err => console.log(err))
    } */

    return dispatch => {
        // const formData = {
        //     id:id,
        //     name : user.name,
        //     email : user.email,
        //     gender :user.gender,
        //     ins_id: user.ins_id,
        //     dob : user.dob,
        //     nid :user.nid,
        //     phone : user.phone,
        //     alt_email :user.alt_email,
        //     address : user.address,
        //     pro_pic : user.pro_pic.files[0],
        //     cover_pic : user.cover_pic.files[0],
        // }
        const fd = new FormData();
        // let pro =null
        // let cover =null;
        // if(user.pro_pic != null)
        // {
        //     pro =user.pro_pic.files[0]
        // }
        // if(user.cover_pic != null)
        // {
        //     cover =user.cover_pic.files[0]
        // }

       
        fd.append('id',id );
        fd.append('name',user.name );
        fd.append('email',user.email );
        fd.append('gender',user.gender );
        fd.append('ins_id',user.ins_id );
        fd.append('dob',user.dob );
        fd.append('nid',user.nid );
        fd.append('phone',user.phone );
        fd.append('alt_email',user.alt_email );
        fd.append('address',user.address );
        if(user.pro_pic != null)
        {
            fd.append('pro_pic',  user.pro_pic.files[0]);
        }
        if(user.cover_pic != null)
        {
            fd.append('cover_pic',   user.cover_pic.files[0]);
        }
        fd.append('_method', 'PATCH');
        axios
        //.patch(`/api/admin/profile/${id}`,formData,{
            .post(`/api/admin/profile/${id}`,fd,{
            headers: {
                'Accept' : 'application/json',
                //'Content-Type' : 'application/x-www-form-urlencoded',
                 'Content-Type' : 'multipart/form-data',
                }
            })
          .then(response => {
            if(response.data.message == "success"){
                alert("update profile done");
                console.log(response.data.message);
            }
            else{
                alert("update profile, something is wrong");
                console.log(response.data.message);
            }
          })
          .catch(err => console.log(err))
    }
}
