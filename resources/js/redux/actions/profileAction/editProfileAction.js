import axios from "axios";
import { readyException } from "jquery";

export const EDIT_USER_PROFILE=(id)=>{
  
    return dispatch => {
        axios
          .get(`/api/admin/profile/${id}/edit`)
          .then(response => {

           
            if(response.data.message == "success"){

                let payload = response.data.data;

                dispatch({
                    type: "EDIT_USER_PROFILE",
                    payload:payload
                })

             

            }
            else{
                alert(response);
            }
          })
          .catch(err => console.log(err))
    }
}
