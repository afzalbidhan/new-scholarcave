import {combineReducers} from 'redux'
import {become_teacher} from './become_teacher'

const becomeTeacherReducers = combineReducers({
    become_teacher,
})
export default becomeTeacherReducers
