import { combineReducers } from "redux";
// import calenderReducer from "./calendar/"
// import emailReducer from "./email/"
// import chatReducer from "./chat/"
// import todoReducer from "./todo/"
// import customizer from "./customizer/"
// import navbar from "./navbar/Index"
// import dataList from "./data-list/"
import auth from "./auth/";
import become_teacher from './become_teacher'
import users from './users/'
import teacher from './teacher'
import profile from './profile'
import course from './course'
import institution from './institution'

const rootReducer = combineReducers({
  auth                      : auth,
  become_teacher            : become_teacher,
  users                     : users,
  teacher                   :teacher,
  profile                   :profile,
  course                    :course,
  institution               :institution,
})

export default rootReducer

