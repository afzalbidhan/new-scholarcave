
export const editProfile = (state={},action) =>{

    switch(action.type){
        case "EDIT_USER_PROFILE":
            return { ...state, values: action.payload }
        default:
            return state;
    }

}
