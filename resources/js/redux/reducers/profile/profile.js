
export const profile = (state={},action) =>{

    switch(action.type){
        case "CURRENT_USER_PROFILE":
            return { ...state, values: action.payload }
         case "EDIT_USER_PROFILE":
            return { ...state, values: action.payload } 
        default:
            return state;
    }

}
