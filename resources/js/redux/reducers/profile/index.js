import {combineReducers} from 'redux'
import {profile} from './profile'
import {editProfile} from './editProfile'

const ProfileReducer = combineReducers({
    profile,editProfile,
})
export default ProfileReducer
