export const login = (state = { userRole: "admin" }, action) => {
  switch (action.type) {
    case "LOGIN_WITH_JWT": {
      return { ...state, values: action.payload }
    }
    case "LOGIN_STATE_UPDATE": {
      return { ...state, values: action.payload }
    }
    case "LOGIN_ERROR": {
      return { ...state, values: action.payload }
    }
    case "LOGOUT_WITH_JWT": {
      return { ...state, values: action.payload }
    }
    case "CHANGE_ROLE": {
      return { ...state, userRole: action.userRole }
    }
    default: {
      return state
    }
  }
}
