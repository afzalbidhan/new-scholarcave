import {combineReducers} from 'redux'
import {getAllUser} from './getAllUser'
import {showIndividualUser} from './showIndividualUsers'

const userReducer = combineReducers({
    getAllUser,
    showIndividualUser,
})
export default userReducer
