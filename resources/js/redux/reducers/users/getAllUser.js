export const getAllUser = (state = {},action) =>{
    
    switch(action.type){
      
        case 'GET_ALL_USER':
            return { ...state, values: action.payload }

        default:
            return state;
    }
}