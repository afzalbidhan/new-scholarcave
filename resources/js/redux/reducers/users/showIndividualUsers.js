export const showIndividualUser = (state = {},action) =>{

    switch(action.type){

        case 'SHOW_INDIVIDUAL_USER':
            return { ...state, values: action.payload }

        default:
            return state;
    }
}
