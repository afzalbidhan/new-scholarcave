export const getAllTeacher = (state = {},action) =>{
    
    switch(action.type){
      
        case 'GET_ALL_TEACHER':
            return { ...state, values: action.payload }

        default:
            return state;
    }
}