

export const teacher = (state = {},action) =>{

    switch(action.type){

        case 'GET_APPROVE_TEACHER':
            return { ...state, values: action.payload }

        default:
            return state;
    }
}
