import {combineReducers} from 'redux'
import {teacher} from './teacher'
import { getAllTeacher } from './getAllTeacher'

const teacherReducer = combineReducers({
    approveteacher: teacher,
    getAllTeacher: getAllTeacher,
})
export default teacherReducer
