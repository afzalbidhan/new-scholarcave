import {combineReducers} from 'redux'
import {getAllCourse} from './getAllCourse'


const courseReducer = combineReducers({
    getAllCourse,

})
export default courseReducer
