export const getAllCourse = (state = {},action) =>{

    switch(action.type){

        case 'GET_ALL_COURSE':
            return { ...state, values: action.payload }
        case 'GET_EDIT_DATA':
            return { ...state, editValues: action.payload }

        default:
            return state;
    }
}
