export const getAllInstitute = (state = {},action) =>{

    switch(action.type){

        case 'GET_ALL_INSTITUTE':
            return { ...state, values: action.payload }

        default:
            return state;
    }
}
