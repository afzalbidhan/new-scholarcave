import React, { useState } from 'react'
import { useDispatch } from 'react-redux';
import { signupWithJWT } from '../../redux/actions/auth/registerActions';
import validateInfo from './validateInfo';

const useForm = (props) => {

    const dispatch = useDispatch();

    const [signupCreds, setSignupCreds] = useState({
        name: "",
        email: '',
        password: "",
        c_password: "",
    });

    const handleChange = (e) => {
        let attrib = e.target.name;
        let value = e.target.value;
        setSignupCreds({ ...signupCreds, [attrib]: value });
    };
    const [errors, setErrors] = useState({});

    const onsubmit = async (e) => {
        e.preventDefault();

        setErrors(validateInfo(signupCreds));

        if (signupCreds.password.length > 7) {
            try {
                await (dispatch(signupWithJWT(signupCreds)))
                props.signupWithJWT(signupCreds);
            }
            catch (e) {
                alert(e)
            }
        }

    };

    const wrongCred = () => {
        alert("something wrong");
        props.values.values = null;
    };

    const correctCred = () => {

        console.log(props.values)

        localStorage.setItem("token", props.values.values.loggedInUser.token);
        localStorage.setItem('user-email', signupCreds.email);
        location.href = "./";
    };


    return {
        wrongCred,
        correctCred,
        onsubmit,
        handleChange,
        errors
    }
}

export default useForm
