export default function validateInfo(values) {

    let error = {};

    // username
    if (!values.name.trim()) {
        error.name = "Username required";
    }

    // email
    if (!values.email) {
        error.email = "Email required";
    } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)) {
        error.email = "Email address is invalid"
    }

    // password
    if (!values.password) {
        error.password = "Password is required";
    } else if (values.password.length < 8) {
        error.password = "Password needs to be 8 characters or more";
    }

    // conform password 
    if (!values.c_password) {
        error.c_password = "Password is required"
    } else if (values.c_password !== values.password) {
        error.c_password = "Password do not match";
    }

    return error;

};