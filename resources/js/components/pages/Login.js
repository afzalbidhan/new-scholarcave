
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from 'react-redux'
import loginAction from '../../redux/actions/auth/'
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { loginWithJWT } from "../../redux/actions/auth/loginActions";

const Login = props => {

  const [data, setData] = React.useState({
    email: "",
    password: "",
    showErrMssg: false,
    emailErr: false,
    passwordErr: false,
    isLoading: true
  });
  const [show, setShow] = useState(false);


  useEffect(() => {



  });

  const { email, password, showErrMssg, emailErr, passwordErr, showToast } = data;

  const onInputChange = e => {
    setData({ ...data, [e.target.name]: e.target.value });
  };

  const onSubmitHandler = async e => {
    e.preventDefault();

    if (email.trim().length == 0 && password.trim().length == 0) {
      setData({ ...data, showErrMssg: true });
    } else if (email.trim().length < 3) {
      setData({ ...data, emailErr: true });
    } else if (password.trim().length < 8) {
      setData({ ...data, passwordErr: true });
    } else {
      try {
        props.loginWithJWT(data);
      } catch (e) {
        alert(e);
      }

      setData({ ...data, emailErr: false, passwordErr: false });
    }
  };


  const wrongCred = () => {
    alert("wrong credentials");
    props.values.values = null;
  };

  const correctCred = () => {



    localStorage.setItem("token", props.values.values.loggedInUser.token);
    //localStorage.setItem("type", props.values.values.loggedInUser.type);
    localStorage.setItem('user-email', data.email)
    localStorage.setItem('user-id', props.values.values.loggedInUser.id)
    localStorage.setItem('user-name', props.values.values.loggedInUser.name)
    location.href = "./";
  };


  return (
    <div className="form_main">
      <div className="container">

        {props.values.values
          ? props.values.values.error == 401
            ? wrongCred()
            : correctCred()
          : null}


        <div className="row">
          <div className="col-md-6 order-1 order-md-2">
            <div className="overlay-container">
              <div className="overlay">
                <div className="overlay-panel overlay-right">
                  <h1>Hello, Friend</h1>
                  <p>Enter your personal details and start your journey with us</p>
                  <Link to="/signup">
                    <button className="ghost" id="signup">
                      Sign up
                    </button>
                  </Link>
                </div>
              </div>
            </div>
          </div>

          <div className="col-md-6 bg order-2 order-md-1">
            <form onSubmit={onSubmitHandler}>
              <h1 className="mb-4">Log in</h1>
              <div className='form-inputs'>
                <input
                  type="text"
                  className='form-input'
                  id="email"
                  name="email"
                  placeholder="Email"
                  onChange={onInputChange}
                  required
                />
              </div>
              <div className='form-inputs'>
                <input
                  className='form-input'
                  type="password"
                  id="password"
                  name="password"
                  placeholder="password"
                  onChange={onInputChange}
                  required
                />
              </div>
              <button className='form-input-btn mt-4' type='submit'>
                Sign In
              </button>
              <p className="mt-5">
                Don't have an Account?  
                <Link to="/signup"> Register <br /> Yourself
                </Link>
              </p>
            </form>
          </div>

        </div>
      </div>
    </div>
  )
}
const mapStateToProps = state => {
  return {
    values: state.auth.login
  };
};
export default connect(mapStateToProps, { loginWithJWT })(Login);