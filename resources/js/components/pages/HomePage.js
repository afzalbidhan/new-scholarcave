import React from "react";
const HomePage = () => {
    return (
        <div>
           <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700" rel="stylesheet" />

            <link rel="stylesheet" href="assets/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossOrigin="anonymous" />
            <link rel="stylesheet" href="assets/css/themify-icons.css" />

            <link rel="stylesheet" href="assets/css/feather.css" />
            <link rel="stylesheet" href="assets/css/home.css" />

		<div className="header-wrapper">
			<div className="container-fluid max-container">
				<div className="row">
					<div className="col-lg-3 col-md-6 col-sm-3 col-xs-6"><a href="index.html" className="logo"><i className="feather-slack text-success"></i><span className=" fredoka-font ls-3 fw-600 text-current font-xl logo-text mb-0 d-none d-md-block">Elomoas. </span> </a></div>
					<div className="col-lg-6 col-md-6 col-sm-6 d-none d-lg-block">
						<ul className="list-inline text-center mb-0 mt-2 pt-1">
							<li className="list-inline-item pl-4 pr-4"><a className="scroll-tiger" href="#feature">Features</a></li>
							<li className="list-inline-item pl-4 pr-4"><a className="scroll-tiger" href="#demo">Demo</a></li>
							<li className="list-inline-item pl-4 pr-4"><a className="scroll-tiger" href="#contact">Contact</a></li>
						</ul>

					</div>
					<div className="col-lg-3 col-md-6 col-sm-3 col-xs-6 text-right">
						<a href="#" className="btn btn-lg btn-primary text-uppercase">Buy Now</a>
					</div>
				</div>
			</div>
		</div>

		<div className="banner-wrapper h100 bscover" style={{backgroundImage: '127.0.0.1:8000/assets/demo/demo-bg.jpg'}}>
			<div className="banner-content">
				<div className="container-fluid max-container">
					<div className="row">
						<div className="col-xl-5 col-md-6 col-sm-8">

							<h2 className="title-text mb-5 mt-5"><b><span>Online</span> Course and LMS HTML Template</b></h2>
							<h4 className="d-inline-block mr-5">60+ <span>HTML Page</span></h4>
							<h4 className="d-inline-block mr-5">14+ <span>Widgets</span></h4>
							<h4 className="d-inline-block mr-5">8+ <span>Home Demo</span></h4>
							<h4 className="d-inline-block mr-5">56+ <span>UI Elements</span></h4>
							<div className="clearfix"></div>
							<a href="#demo" className="btn btn-lg btn-primary mr-4 text-uppercase mt-5">See DEMOs</a>


							<div className="icon-scroll pos-bottom-center icon-white"></div>
						</div>

					</div>
				</div>
			</div>
		</div>



		<div className="section pb50 pt100" id="feature">
			<div className="container">
				<div className="row justify-content-center">
					<div className="col-md-8 col-lg-6 text-center">
						<h2 className="title-text2 mb-4"><b>Explore Feature</b></h2>
						<p>Zipto powers thousands of apps at some of the smartest companies around the world. Be a part of professional community.</p>
					</div>
				</div>
				<div className="row">
					<div className="col-sm-12 pt100"></div>
					<div className="col-xl-4 col-lg-6">
						<div className="icon-div mb80">
							<i className="ti-gift"></i>
							<h3>Developer Friendly</h3>
							<p>Reproduction of the most popular apps, such as Google Analytics, DIgitalOcean, Worpdress, Todoist, etc</p>
						</div>
					</div>
					<div className="col-xl-4 col-lg-6">
						<div className="icon-div mb80">
							<i className="ti-reload"></i>
							<h3>Documentation</h3>
							<p>Reproduction of the most popular apps, such as Google Analytics, DIgitalOcean, Worpdress, Todoist, etc</p>
						</div>
					</div>
					<div className="col-xl-4 col-lg-6">
						<div className="icon-div mb80">
							<i className="ti-medall-alt"></i>
							<h3>Quality Code</h3>
							<p>Reproduction of the most popular apps, such as Google Analytics, DIgitalOcean, Worpdress, Todoist, etc</p>
						</div>
					</div>
					<div className="col-xl-4 col-lg-6">
						<div className="icon-div mb80">
							<i className="ti-comments"></i>
							<h3>24/7 Support</h3>
							<p>Reproduction of the most popular apps, such as Google Analytics, DIgitalOcean, Worpdress, Todoist, etc</p>
						</div>
					</div>
					<div className="col-xl-4 col-lg-6">
						<div className="icon-div mb80">
							<i className="ti-panel"></i>
							<h3>Dark Mode</h3>
							<p>Reproduction of the most popular apps, such as Google Analytics, DIgitalOcean, Worpdress, Todoist, etc</p>
						</div>
					</div>
					<div className="col-xl-4 col-lg-6">
						<div className="icon-div mb80">
							<i className="ti-user"></i>
							<h3>User Friendly</h3>
							<p>Reproduction of the most popular apps, such as Google Analytics, DIgitalOcean, Worpdress, Todoist, etc</p>
						</div>
					</div>
					<div className="col-xl-4 col-lg-6">
						<div className="icon-div mb80">
							<i className="ti-bar-chart"></i>
							<h3>Clean Component</h3>
							<p>Reproduction of the most popular apps, such as Google Analytics, DIgitalOcean, Worpdress, Todoist, etc</p>
						</div>
					</div>
					<div className="col-xl-4 col-lg-6">
						<div className="icon-div mb80">
							<i className="ti-package"></i>
							<h3>12 Color  Option</h3>
							<p>Reproduction of the most popular apps, such as Google Analytics, DIgitalOcean, Worpdress, Todoist, etc</p>
						</div>
					</div>
					<div className="col-xl-4 col-lg-6">
						<div className="icon-div mb80">
							<i className="ti-stats-down"></i>
							<h3>Apps Pages</h3>
							<p>Reproduction of the most popular apps, such as Google Analytics, DIgitalOcean, Worpdress, Todoist, etc</p>
						</div>
					</div>

				</div>
			</div>
		</div>

		<div className="demo p100" id="demo" style={{backgroundColor: '#f3fbff'}}>
			<div className="container-fluid max-container">
				<div className="row justify-content-center">
					<div className="col-md-8 col-lg-6 col-xl-4 text-center">
						<h2 className="title-text2 mb-4"><b>Choose Demo</b></h2>
						<p>Create a really awesome website, choose the version that will suit your requirements in a best way.</p>
					</div>
					<div className="clearfix"></div>
					<div className="col-lg-12 mt-4 text-center">
						<ul className="nav nav-tabs tabs-icon list-inline d-block w-100 text-center border-bottom-0" id="myNavTabs">
                            <li className="active list-inline-item"><a className="fw-900 font-xssss text-black text-uppercase active" href="#navtabs1" data-toggle="tab">Demo</a></li>
                            <li className="list-inline-item"><a className="fw-900 font-xssss text-black text-uppercase" href="#navtabs2" data-toggle="tab">Home</a></li>
	                        <li className="list-inline-item"><a className="fw-900 font-xssss text-black text-uppercase" href="#navtabs3" data-toggle="tab">Shop</a></li>
	                        <li className="list-inline-item"><a className="fw-900 font-xssss text-black text-uppercase" href="#navtabs4" data-toggle="tab">Other</a></li>
	                    </ul>
					</div>
				</div>
				<div className="row">
					<div className="col-lg-12 pt100">
						<div className="tab-content">
							<div className="tab-pane fade active show" id="navtabs1">
								<div className="row">
									<div className="col-lg-4 col-md-6 demo-item">
										<img src="assets/demo/d-1.jpg" alt="demo-image" className="w-100" />
										<span>Default</span>
										<div className="btn-wrap">
											<a target="_blank" href="default.html">LTR</a>
											<a target="_blank" href="rtl/default.html">RTL</a>
										</div>
									</div>
									<div className="col-lg-4 col-md-6 demo-item">
										<img src="assets/demo/d-34.jpg" alt="demo-image" className="w-100" />
										<span>Category</span>
										<div className="btn-wrap">
											<a target="_blank" href="default-categories.html">LTR</a>
											<a target="_blank" href="rtl/default-categories.html">RTL</a>
										</div>
									</div>

									<div className="col-lg-4 col-md-6 demo-item">
										<img src="assets/demo/d-21.jpg" alt="demo-image" className="w-100" />
										<span>Search</span>
										<div className="btn-wrap">
											<a target="_blank" href="default-search.html">LTR</a>
											<a target="_blank" href="rtl/default-search.html">RTL</a>
										</div>
									</div>

									<div className="col-lg-4 col-md-6 demo-item">
										<img src="assets/demo/d-2.jpg" alt="demo-image" className="w-100" />
										<span>Member</span>
										<div className="btn-wrap">
											<a target="_blank" href="default-follower.html">LTR</a>
											<a target="_blank" href="rtl/default-follower.html">RTL</a>
										</div>
									</div>

									<div className="col-lg-4 col-md-6 demo-item">
										<img src="assets/demo/d-3.jpg" alt="demo-image" className="w-100" />
										<span>Channel</span>
										<div className="btn-wrap">
											<a target="_blank" href="default-channel.html">LTR</a>
											<a target="_blank" href="rtl/default-channel.html">RTL</a>
										</div>
									</div>

									<div className="col-lg-4 col-md-6 demo-item">
										<img src="assets/demo/d-4.jpg" alt="demo-image" className="w-100" />
										<span>Live Stream</span>
										<div className="btn-wrap">
											<a target="_blank" href="default-live-stream.html">LTR</a>
											<a target="_blank" href="rtl/default-live-stream.html">RTL</a>
										</div>
									</div>

									<div className="col-lg-4 col-md-6 demo-item">
										<img src="assets/demo/d-5.jpg" alt="demo-image" className="w-100" />
										<span>User Profile</span>
										<div className="btn-wrap">
											<a target="_blank" href="default-user-profile.html">LTR</a>
											<a target="_blank" href="rtl/default-user-profile.html">RTL</a>
										</div>
									</div>

									<div className="col-lg-4 col-md-6 demo-item">
										<img src="assets/demo/d-6.jpg" alt="demo-image" className="w-100" />
										<span>Author Profile</span>
										<div className="btn-wrap">
											<a target="_blank" href="default-author-profile.html">LTR</a>
											<a target="_blank" href="rtl/default-author-profile.html">RTL</a>
										</div>
									</div>

									<div className="col-lg-4 col-md-6 demo-item">
										<img src="assets/demo/d-7.jpg" alt="demo-image" className="w-100" />
										<span>Course Single 1</span>
										<div className="btn-wrap">
											<a target="_blank" href="default-course-details.html">LTR</a>
											<a target="_blank" href="rtl/default-course-details.html">RTL</a>
										</div>
									</div>

									<div className="col-lg-4 col-md-6 demo-item">
										<img src="assets/demo/d-8.jpg" alt="demo-image" className="w-100" />
										<span>Course Single 2</span>
										<div className="btn-wrap">
											<a target="_blank" href="default-course-details-2.html">LTR</a>
											<a target="_blank" href="rtl/default-course-details-2.html">RTL</a>
										</div>
									</div>

									<div className="col-lg-4 col-md-6 demo-item">
										<img src="assets/demo/d-10.jpg" alt="demo-image" className="w-100" />
										<span>Test</span>
										<div className="btn-wrap">
											<a target="_blank" href="default-test.html">LTR</a>
											<a target="_blank" href="rtl/default-test.html">RTL</a>
										</div>
									</div>

									<div className="col-lg-4 col-md-6 demo-item">
										<img src="assets/demo/d-11.jpg" alt="demo-image" className="w-100" />
										<span>Analytics</span>
										<div className="btn-wrap">
											<a target="_blank" href="default-analytics.html">LTR</a>
											<a target="_blank" href="rtl/default-analytics.html">RTL</a>
										</div>
									</div>



									<div className="col-lg-4 col-md-6 demo-item">
										<img src="assets/demo/d-9.jpg" alt="demo-image" className="w-100" />
										<span>Email Box</span>
										<div className="btn-wrap">
											<a target="_blank" href="email-box.html">LTR</a>
											<a target="_blank" href="rtl/email-box.html">RTL</a>
										</div>
									</div>

									<div className="col-lg-4 col-md-6 demo-item">
										<img src="assets/demo/d-13.jpg" alt="demo-image" className="w-100" />
										<span>Chat</span>
										<div className="btn-wrap">
											<a target="_blank" href="message.html">LTR</a>
											<a target="_blank" href="rtl/message.html">RTL</a>
										</div>
									</div>

									<div className="col-lg-4 col-md-6 demo-item">
										<img src="assets/demo/d-14.jpg" alt="demo-image" className="w-100" />
										<span>To Do</span>
										<div className="btn-wrap">
											<a target="_blank" href="todo.html">LTR</a>
											<a target="_blank" href="rtl/todo.html">RTL</a>
										</div>
									</div>


									<div className="col-lg-4 col-md-6 demo-item">
										<img src="assets/demo/d-17.jpg" alt="demo-image" className="w-100" />
										<span>Settings</span>
										<div className="btn-wrap">
											<a target="_blank" href="default-settings.html">LTR</a>
											<a target="_blank" href="rtl/default-settings.html">RTL</a>
										</div>
									</div>

									<div className="col-lg-4 col-md-6 demo-item">
										<img src="assets/demo/d-15.jpg" alt="demo-image" className="w-100" />
										<span>Contact</span>
										<div className="btn-wrap">
											<a target="_blank" href="contact-information.html">LTR</a>
											<a target="_blank" href="rtl/contact-information.html">RTL</a>
										</div>
									</div>

									<div className="col-lg-4 col-md-6 demo-item">
										<img src="assets/demo/d-16.jpg" alt="demo-image" className="w-100" />
										<span>Account</span>
										<div className="btn-wrap">
											<a target="_blank" href="account-information.html">LTR</a>
											<a target="_blank" href="rtl/account-information.html">RTL</a>
										</div>
									</div>

									<div className="col-lg-4 col-md-6 demo-item">
										<img src="assets/demo/d-19.jpg" alt="demo-image" className="w-100" />
										<span>Payment</span>
										<div className="btn-wrap">
											<a target="_blank" href="payment.html">LTR</a>
											<a target="_blank" href="rtl/payment.html">RTL</a>
										</div>
									</div>

									<div className="col-lg-4 col-md-6 demo-item">
										<img src="assets/demo/d-18.jpg" alt="demo-image" className="w-100" />
										<span>Password</span>
										<div className="btn-wrap">
											<a target="_blank" href="password.html">LTR</a>
											<a target="_blank" href="rtl/password.html">RTL</a>
										</div>
									</div>

									<div className="col-lg-4 col-md-6 demo-item">
										<img src="assets/demo/d-20.jpg" alt="demo-image" className="w-100" />
										<span>Social</span>
										<div className="btn-wrap">
											<a target="_blank" href="social.html">LTR</a>
											<a target="_blank" href="rtl/social.html">RTL</a>
										</div>
									</div>

									<div className="col-lg-4 col-md-6 demo-item">
										<img src="assets/demo/d-12.jpg" alt="demo-image" className="w-100" />
										<span>Popup Chat</span>
										<div className="btn-wrap">
											<a target="_blank" href="popup-chat.html">LTR</a>
											<a target="_blank" href="rtl/popup-chat.html">RTL</a>
										</div>
									</div>
								</div>
							</div>
							<div className="tab-pane fade" id="navtabs2">
								<div className="row">
									<div className="col-lg-4 col-md-6 demo-item">
										<img src="assets/demo/h-3.jpg" alt="demo-image" className="w-100" />
										<span>Home 1</span>
										<div className="btn-wrap">
											<a target="_blank" href="home-3.html">LTR</a>
											<a target="_blank" href="rtl/home-3.html">RTL</a>
										</div>
									</div>



				                    <div className="col-lg-4 col-md-6 demo-item">
				                        <img src="assets/demo/h-2.jpg" alt="demo-image" className="w-100" />
										<span>Home 3</span>
										<div className="btn-wrap">
											<a target="_blank" href="home-2.html">LTR</a>
											<a target="_blank" href="rtl/home-2.html">RTL</a>
										</div>
				                    </div>

									<div className="col-lg-4 col-md-6 demo-item">
										<img src="assets/demo/h-4.jpg" alt="demo-image" className="w-100" />
										<span>Home 4</span>
										<div className="btn-wrap">
											<a target="_blank" href="home-4.html">LTR</a>
											<a target="_blank" href="rtl/home-4.html">RTL</a>
										</div>
									</div>

									<div className="col-lg-4 col-md-6 demo-item">
										<img src="assets/demo/h-6.jpg" alt="demo-image" className="w-100" />
										<span>Home 5</span>
										<div className="btn-wrap">
											<a target="_blank" href="home-5.html">LTR</a>
											<a target="_blank" href="rtl/home-5.html">RTL</a>
										</div>
									</div>

									<div className="col-lg-4 col-md-6 demo-item">
										<img src="assets/demo/h-5.jpg" alt="demo-image" className="w-100" />
										<span>Home 6</span>
										<div className="btn-wrap">
											<a target="_blank" href="home-6.html">LTR</a>
											<a target="_blank" href="rtl/home-6.html">RTL</a>
										</div>
									</div>

									<div className="col-lg-4 col-md-6 demo-item">
										<img src="assets/demo/h-7.jpg" alt="demo-image" className="w-100" />
										<span>Courses Grid 1</span>
										<div className="btn-wrap">
											<a target="_blank" href="courses-grid-1.html">LTR</a>
											<a target="_blank" href="rtl/courses-grid-1.html">RTL</a>
										</div>
									</div>

									<div className="col-lg-4 col-md-6 demo-item">
										<img src="assets/demo/h-8.jpg" alt="demo-image" className="w-100" />
										<span>Courses Grid 2</span>
										<div className="btn-wrap">
											<a target="_blank" href="courses-grid-2.html">LTR</a>
											<a target="_blank" href="rtl/courses-grid-2.html">RTL</a>
										</div>
									</div>

									<div className="col-lg-4 col-md-6 demo-item">
										<img src="assets/demo/h-9.jpg" alt="demo-image" className="w-100" />
										<span>Courses Grid 3</span>
										<div className="btn-wrap">
											<a target="_blank" href="courses-grid-3.html">LTR</a>
											<a target="_blank" href="rtl/courses-grid-3.html">RTL</a>
										</div>
									</div>



									<div className="col-lg-4 col-md-6 demo-item">
										<img src="assets/demo/h-10.jpg" alt="demo-image" className="w-100" />
										<span>Course Single</span>
										<div className="btn-wrap">
											<a target="_blank" href="course-details.html">LTR</a>
											<a target="_blank" href="rtl/course-details.html">RTL</a>
										</div>
									</div>

									<div className="col-lg-4 col-md-6 demo-item">
										<img src="assets/demo/h-11.jpg" alt="demo-image" className="w-100" />
										<span>Course Single 2</span>
										<div className="btn-wrap">
											<a target="_blank" href="course-details-2.html">LTR</a>
											<a target="_blank" href="rtl/course-details-2.html">RTL</a>
										</div>
									</div>

									<div className="col-lg-4 col-md-6 demo-item">
										<img src="assets/demo/h-13.jpg" alt="demo-image" className="w-100" />
										<span>Author Profile</span>
										<div className="btn-wrap">
											<a target="_blank" href="user-profile.html">LTR</a>
											<a target="_blank" href="rtl/user-profile.html">RTL</a>
										</div>
									</div>

									<div className="col-lg-4 col-md-6 demo-item">
										<img src="assets/demo/h-12.jpg" alt="demo-image" className="w-100" />
										<span>User Profile</span>
										<div className="btn-wrap">
											<a target="_blank" href="author-profile.html">LTR</a>
											<a target="_blank" href="rtl/author-profile.html">RTL</a>
										</div>
									</div>





				                </div>
							</div>
							<div className="tab-pane fade" id="navtabs3">
								<div className="row">
									<div className="col-lg-4 col-md-6 demo-item">
				                        <img src="assets/demo/h-1.jpg" alt="demo-image" className="w-100" />
										<span>Home 2</span>
										<div className="btn-wrap">
											<a target="_blank" href="home-1.html">LTR</a>
											<a target="_blank" href="rtl/home-1.html">RTL</a>
										</div>
				                    </div>

				                    <div className="col-lg-4 col-md-6 demo-item">
				                        <img src="assets/demo/shop-1.jpg" alt="demo-image" className="w-100" />
			                            <span>Shop 1</span>
			                            <div className="btn-wrap">
			                            	<a target="_blank" href="shop-2.html">LTR</a>
			                            	<a target="_blank" href="rtl/shop-2.html">RTL</a>
			                            </div>
				                    </div>

				                    <div className="col-lg-4 col-md-6 demo-item">
				                        <img src="assets/demo/shop-2.jpg" alt="demo-image" className="w-100" />
			                            <span>Shop 2</span>
			                            <div className="btn-wrap">
			                            	<a target="_blank" href="shop-3.html">LTR</a>
			                            	<a target="_blank" href="rtl/shop-3.html">RTL</a>
			                            </div>
				                    </div>

									<div className="col-lg-4 col-md-6 demo-item">
										<img src="assets/demo/shop-3.jpg" alt="demo-image" className="w-100" />
										<span>Shop 3</span>
										<div className="btn-wrap">
											<a target="_blank" href="shop-1.html">LTR</a>
											<a target="_blank" href="rtl/shop-1.html">RTL</a>
										</div>
									</div>



									<div className="col-lg-4 col-md-6 demo-item">
										<img src="assets/demo/single-product-3.jpg" alt="demo-image" className="w-100" />
										<span>Single Product 1</span>
										<div className="btn-wrap">
											<a target="_blank" href="single-product.html">LTR</a>
											<a target="_blank" href="rtl/single-product.html">RTL</a>
										</div>
									</div>

									<div className="col-lg-4 col-md-6 demo-item">
										<img src="assets/demo/single-product-2.jpg" alt="demo-image" className="w-100" />
										<span>Single Product 2</span>
										<div className="btn-wrap">
											<a target="_blank" href="single-product-2.html">LTR</a>
											<a target="_blank" href="rtl/single-product-2.html">RTL</a>
										</div>
									</div>
									<div className="col-lg-4 col-md-6 demo-item">
										<img src="assets/demo/single-product.jpg" alt="demo-image" className="w-100" />
										<span>Single Product 3</span>
										<div className="btn-wrap">
											<a target="_blank" href="single-product-3.html">LTR</a>
											<a target="_blank" href="rtl/single-product-3.html">RTL</a>
										</div>
									</div>



									<div className="col-lg-4 col-md-6 demo-item">
										<img src="assets/demo/cart.jpg" alt="demo-image" className="w-100" />
										<span>Cart</span>
										<div className="btn-wrap">
											<a target="_blank" href="cart.html">LTR</a>
											<a target="_blank" href="rtl/cart.html">RTL</a>
										</div>
									</div>

									<div className="col-lg-4 col-md-6 demo-item">
										<img src="assets/demo/checkout.jpg" alt="demo-image" className="w-100" />
										<span>Checkout</span>
										<div className="btn-wrap">
											<a target="_blank" href="checkout.html">LTR</a>
											<a target="_blank" href="rtl/checkout.html">RTL</a>
										</div>
									</div>


				                </div>
							</div>

							<div className="tab-pane fade" id="navtabs4">
								<div className="row">
									<div className="col-lg-4 col-md-6 demo-item">
				                        <img src="assets/demo/login.jpg" alt="demo-image" className="w-100" />
										<span>Login</span>
										<div className="btn-wrap">
											<a target="_blank" href="login.html">LTR</a>
											<a target="_blank" href="rtl/login.html">RTL</a>
										</div>
				                    </div>

				                    <div className="col-lg-4 col-md-6 demo-item">
				                        <img src="assets/demo/register.jpg" alt="demo-image" className="w-100" />
			                            <span>Register</span>
			                            <div className="btn-wrap">
			                            	<a target="_blank" href="register.html">LTR</a>
			                            	<a target="_blank" href="rtl/register.html">RTL</a>
			                            </div>
				                    </div>

				                    <div className="col-lg-4 col-md-6 demo-item">
				                        <img src="assets/demo/forgot.jpg" alt="demo-image" className="w-100" />
			                            <span>Forgot</span>
			                            <div className="btn-wrap">
			                            	<a target="_blank" href="forgot.html">LTR</a>
			                            	<a target="_blank" href="rtl/forgot.html">RTL</a>
			                            </div>
				                    </div>

									<div className="col-lg-4 col-md-6 demo-item">
										<img src="assets/demo/coming-soon.jpg" alt="demo-image" className="w-100" />
										<span>Coming Soon</span>
										<div className="btn-wrap">
											<a target="_blank" href="coming-soon.html">LTR</a>
											<a target="_blank" href="rtl/coming-soon.html">RTL</a>
										</div>
									</div>

									<div className="col-lg-4 col-md-6 demo-item">
										<img src="assets/demo/not-found.jpg" alt="demo-image" className="w-100" />
										<span>404</span>
										<div className="btn-wrap">
											<a target="_blank" href="404.html">LTR</a>
											<a target="_blank" href="rtl/404.html">RTL</a>
										</div>
									</div>



									<div className="col-lg-4 col-md-6 demo-item">
										<img src="assets/demo/blog.jpg" alt="demo-image" className="w-100" />
										<span>Blog</span>
										<div className="btn-wrap">
											<a target="_blank" href="blog.html">LTR</a>
											<a target="_blank" href="rtl/blog.html">RTL</a>
										</div>
									</div>

									<div className="col-lg-4 col-md-6 demo-item">
										<img src="assets/demo/blog-sidebar.jpg" alt="demo-image" className="w-100" />
										<span>Blog Sidebar</span>
										<div className="btn-wrap">
											<a target="_blank" href="blog-sidebar.html">LTR</a>
											<a target="_blank" href="rtl/blog-sidebar.html">RTL</a>
										</div>
									</div>
									<div className="col-lg-4 col-md-6 demo-item">
										<img src="assets/demo/blog-single.jpg" alt="demo-image" className="w-100" />
										<span>Blog Single</span>
										<div className="btn-wrap">
											<a target="_blank" href="blog-single.html">LTR</a>
											<a target="_blank" href="rtl/blog-single.html">RTL</a>
										</div>
									</div>



									<div className="col-lg-4 col-md-6 demo-item">
										<img src="assets/demo/contact.jpg" alt="demo-image" className="w-100" />
										<span>Contact</span>
										<div className="btn-wrap">
											<a target="_blank" href="contact.html">LTR</a>
											<a target="_blank" href="rtl/contact.html">RTL</a>
										</div>
									</div>

									<div className="col-lg-4 col-md-6 demo-item">
										<img src="assets/demo/contact-two.jpg" alt="demo-image" className="w-100" />
										<span>Contact 2</span>
										<div className="btn-wrap">
											<a target="_blank" href="contact-two.html">LTR</a>
											<a target="_blank" href="rtl/contact-two.html">RTL</a>
										</div>
									</div>

									<div className="col-lg-4 col-md-6 demo-item">
										<img src="assets/demo/about.jpg" alt="demo-image" className="w-100" />
										<span>About</span>
										<div className="btn-wrap">
											<a target="_blank" href="about.html">LTR</a>
											<a target="_blank" href="rtl/about.html">RTL</a>
										</div>
									</div>

									<div className="col-lg-4 col-md-6 demo-item">
										<img src="assets/demo/service.jpg" alt="demo-image" className="w-100" />
										<span>Service</span>
										<div className="btn-wrap">
											<a target="_blank" href="service.html">LTR</a>
											<a target="_blank" href="rtl/service.html">RTL</a>
										</div>
									</div>

									<div className="col-lg-4 col-md-6 demo-item">
										<img src="assets/demo/price.jpg" alt="demo-image" className="w-100" />
										<span>Price</span>
										<div className="btn-wrap">
											<a target="_blank" href="price.html">LTR</a>
											<a target="_blank" href="rtl/price.html">RTL</a>
										</div>
									</div>


				                </div>
							</div>




						</div>
					</div>




				</div>
			</div>
		</div>




        </div>
    );
};

export default HomePage;
