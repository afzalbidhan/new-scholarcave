import React, { Component } from "react";
import ReactDOM from "react-dom";
import Wrapper from "../../utils/Wrapper";
import NoLayoutContent from "../../utils/NoLayoutContent";
//import "bootstrap/dist/css/bootstrap.min.css";
import { BrowserRouter as Router, Route, Switch, Link } from "react-router-dom";
import store from "../../redux/storeConfig/store";
import { Provider } from "react-redux";
import BecomeTeacher from "./BecomeTeacher";
import LayoutContent from "../../utils/LayoutContent";

//import AllRoutes from "../../utils/AllRoutes";
export default class Home extends Component {
    constructor() {
        super();

        let token = localStorage.getItem("token");

        this.state = {
            count: 0,
            isLoggedIn: token
        };
    }

    render() {

        return (
            <Router>

                 <Switch>
                    { this.state.isLoggedIn ? <Wrapper /> : <NoLayoutContent /> }
                 </Switch>
            </Router>
        );
    }
}

const rootElement = document.getElementById("app");
ReactDOM.render(
    <Provider store={store}>
        <Home />
    </Provider>,
    rootElement
);
