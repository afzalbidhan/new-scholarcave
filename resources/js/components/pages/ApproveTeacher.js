import React, { useEffect,useState } from "react";
import { connect } from "react-redux";
import BreadCrumb from "../../utils/Layouts/Partial/BreadCrumb";
import { GET_APPROVE_TEACHER,APPROVE_TEACHER } from "../../redux/actions/teacherAction/teacherAction";


const ApproveTeacher = (props) => {

    const [count,setCount] = useState(0)

    let jsxData =[];


    useEffect(() => {
        props.GET_APPROVE_TEACHER()

    }, [count])

    const jsxFunc = () => {
        jsxData = [];
        if(props.values.values){
          props.values.values.map((item, valkey) => {
            jsxData.push(
                <tr key={valkey}>
                    <td className="budget">{valkey + 1}</td>
                    <td className="budget">{item.name}</td>
                    {/* <td className="budget">{item.email}</td>
                    <td className="budget">{item.phone}</td> */}

                    <td>
                        <span className="badge badge-dot mr-4">
                            <i className="bg-warning"></i>
                            <span className="status">pending </span>
                        </span>
                    </td>


                    <td>{item.created_at}</td>
                    <td><button className="btn btn-success btn-sm "
                    onClick={()=>{approveteacher(item.id)}}> Make Teacher</button></td>
                </tr>
            );
        });
       }
    };

    const approveteacher = async (id) =>{
        //console.log(id)
        setCount(count+1);
        await(props.APPROVE_TEACHER(id))

    }

    const openModal = () => {
        alert("modal open");
    };

    return (
        <div onLoad={jsxFunc()}>
            <BreadCrumb title={"Teachers"} openModal={openModal} />


            <div className="container-fluid mt--6">
                <div className="row text-center">
                    <div className="col">
                        <div className="card">
                            <div className="card-header border-0">
                                <h3 className="mb-0">Teacher Approved Status</h3>
                            </div>
                            
                            <div className="table-responsive">
                            <table className="table align-items-center table-flush">
                                <thead className="thead-light">
                                    <tr >
                                        <th
                                            scope="col"
                                            className="sort"
                                            data-sort="name"
                                        >
                                            SL
                                        </th>
                                        <th
                                            scope="col"
                                            className="sort"
                                            data-sort="budget"
                                        >
                                            Name
                                        </th>
                                        {/* <th
                                            scope="col"
                                            className="sort"
                                            data-sort="status"
                                        >
                                            Email
                                        </th>
                                        <th scope="col">Phone</th> */}
                                        <th
                                            scope="col"
                                            className="sort"
                                            data-sort="completion"
                                        >
                                            Status
                                        </th>



                                        <th scope="col">Created Date</th>

                                        <th
                                            scope="col"
                                            className="sort"
                                            data-sort="budget"
                                        >
                                            Action
                                        </th>

                                    </tr>
                                </thead>
                                <tbody className="list">

                                    {jsxData}
                                </tbody>
                            </table></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

const mapStateToProps = state => {
    return {
        values: state.teacher.approveteacher
    };
};

export default connect( mapStateToProps , {GET_APPROVE_TEACHER, APPROVE_TEACHER}) (ApproveTeacher)
