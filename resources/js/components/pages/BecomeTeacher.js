import React from 'react'
import { connect } from "react-redux";
import { BECOME_TEACHER } from '../../redux/actions/become_teacher/become_teacherAction'
import { Link } from 'react-router-dom'
import Sidebar from '../../utils/Layouts/Partial/Sidebar';
import Search from '../../utils/Layouts/Partial/Search';
import RightSideMenu from '../../utils/Layouts/Partial/RightSideMenu';
import RightSideImageName from '../../utils/Layouts/Partial/RightSideImageName';
import BreadCrumb from '../../utils/Layouts/Partial/BreadCrumb';
import BreadCrumbRightSide from '../../utils/Layouts/Partial/BreadCrumbRightSide';
import TopNavbar from '../../utils/Layouts/Partial/TopNavbar';


const BecomeTeacher = (props) => {
    const email = localStorage.getItem('user-email');
    //  console.log(email)


    const BecomeTeacher = () => {
        props.BECOME_TEACHER({ email: email });
    }



    return (
        <div>
            <div className="main-content" id="panel">
                <BreadCrumb title={"Become a Teacher"} />
                <div className="header pb-6 d-flex align-items-center">
                    <div className="container-fluid d-flex align-items-center">
                        <div className="row p-3">
                            <div className="col-lg-12 col-md-12">
                                <div className="row">
                                    <div className="col-12 col-md-12 col-lg-6 order-last order-lg-first mt-3 mt-lg-0">
                                        <h2 className="display-3 text-black">Become A Teacher</h2>
                                        <p className="text-black mt-0 mb-4">Welcome we are happy to have you!</p>
                                        < p className="text-black mt-1 mb-3">We are building a world class tutoring service made of compassionate, caring individual like you.
                                            Thank you for offering your support.</p>
                                        <h2 className="text-black mt-0 mb-3" >How does it work?</h2>
                                        <p className="text-black mt-0 mb-3">Getting started as an active teacher is easy and rewarding in many ways. You are in complete control over
                                            your displayed availability, so you can listen as often as you like. </p>
                                        <h3 className="display-5 text-black">Here is the two-step registration process:</h3>
                                        <p className="text-black mt-0 mb-3">1. Complete the active tutoring course</p>
                                        <p className="text-black mt-0 mb-5">2. Create your teacher profile</p>
                                        <button className="btn btn-primary" onClick={() => { BecomeTeacher() }}> Yes, Confirm</button>
                                        <br /> <br />
                                    </div>
                                    <div className="col-12 col-lg-6 col-md-12">
                                        <div>
                                        <img  alt="Image placeholder" src="argon/assets/img/theme/become_a_teacher.jpg" className="w-100 mt-3"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}


const mapStateToProps = state => {
    return {
        values: state.become_teacher.become_teacher
    };
};

export default connect(mapStateToProps, { BECOME_TEACHER })(BecomeTeacher)
