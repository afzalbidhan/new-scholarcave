import React, { useEffect, useState } from 'react'
import BreadCrumb from "../../utils/Layouts/Partial/BreadCrumb";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { EDIT_USER_PROFILE } from "../../redux/actions/profileAction/profileAction";
import { UPDATE_USER_PROFILE } from "../../redux/actions/profileAction/profileAction";
import { useParams } from 'react-router-dom';
import { Redirect } from 'react-router-dom';

const EditProfile = (props) => {
   
    const { id } = useParams();

    const [editData, setEditData] = useState({
        name: '',
        email: '',
        gender: '',
        ins_id: '',
        dob: '',
        nid:   '',
        phone: '',
        alt_email:  '' ,
        address: ''

    })



   
     
    useEffect(() => {
       
       
        props.EDIT_USER_PROFILE(id)
      
      
        

      
    },[])



    
    let jsxData = {};


    const jsxFunc = () => {

        
        if (props.values.values) {

            
            jsxData = props.values.values

        }


        if(jsxData.general_users){
             

                 let name = jsxData.name;
                 let email = jsxData.email;
                 let gender = jsxData.general_users.gender;
                 let ins_id = null;
                 let dob = jsxData.general_users.dob;
                 let nid =   jsxData.general_users.nid;
                 let phone = jsxData.phone;
                 let alt_email =  jsxData.general_users.alt_email;
                 let address =  jsxData.general_users.address ;

            setEditData({...editData,name,email,gender,ins_id,dob,nid,phone,alt_email,address});
        }
       
    }


    if(editData.name == ''){
        jsxFunc();
    }
   
   

    


    // const [editData, setEditData] = useState({
    //     name: jsxData.name,
    //     email: jsxData.email,
    //     gender: jsxData.gender,
    //     ins_id: '',
    //     dob: !! jsxData.general_users.dob ? jsxData.general_users.dob :'',
    //     nid:  !! jsxData.general_users.nid  ? jsxData.general_users.nid : '',
    //     phone: jsxData.phone,
    //     alt_email: !! jsxData.general_users.alt_email ? jsxData.general_users.alt_email : '' ,
    //     address: !! jsxData.general_users.address ? jsxData.general_users.address :''

    // })
    
    const onInputChange = (e) => {
        let user = { ...editData, [e.target.name]: e.target.value }
        setEditData(user);
    }

    ///file

    const [selectFile, setSelectFile] = useState({
        pro_pic: null,
        cover_pic: null,
    })

    const onInputChangeFile = (e) => {
        let user = { ...selectFile, [e.target.name]: e.target }
        setSelectFile(user);
    }

 

    // function validateEmail(email) {
    //     const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    //     return re.test(String(email).toLowerCase());
    // }

    const submitForm = (e) => {
        e.preventDefault();

        // if(editData.name.length>3 && editData.gender != null && editData.ins_id != null && editData.dob != null &&
        //     editData.nid.length>5 && editData.phone.length == 11 && editData.address != null)
        //     {
        // if (validateEmail(editData.email) && validateEmail(editData.alt_email)) {
        //     // alert('success')

        //     // location.href = "/profile";
            let user = { ...editData, ...selectFile }
            props.UPDATE_USER_PROFILE(id, user);

            // props.setEditTimes(props.editTimes+1)
             location.href= '/profile'
            // return <Redirect to='/profile'/>
        // }
        // else {
        //     alert('Give proper email')
        // }
        // }
        // else
        // {
        //     alert('Data can not be empty')
        // }

        /* if(editData.name == ''){
            alert('Give proper email')
        }else{
            let user = { ...editData, ...selectFile }
            props.UPDATE_USER_PROFILE(id, user);
        } */
    }

    return (

        <div >
            <BreadCrumb title={"Edit Profile"} />
{console.log(jsxData)}
            <div className="container-fluid mt--6">
                <div className="row">
                    <div className="col-xl-12 order-xl-1">
                        <div className="card">
                            <div className="card-header">
                                <div className="row align-items-center">
                                    <div className="col-8">
                                        <h3 className="mb-0">Edit profile </h3>
                                    </div>
                                </div>
                            </div>
                            <div className="card-body">
                                <form onSubmit={submitForm} encType="multipart/form-data">
                                    <h6 className="heading-small text-muted mb-4">User information</h6>
                                    <div className="pl-lg-4">
                                        <div className="row">

                                            {/* name */}
                                            <div className="col-lg-6">
                                                <div className="form-group">
                                                    <label className="form-control-label" htmlFor="name">Name</label>
                                                    <input type="text" id="name" name="name" className="form-control" defaultValue={editData.name} onChange={onInputChange} required/>
                                                </div>
                                            </div>

                                            {/* email */}
                                            <div className="col-lg-6">
                                                <div className="form-group">
                                                    <label className="form-control-label" htmlFor="email">Email address</label>
                                                    <input type="email" id="email" name="email" className="form-control" defaultValue={editData.email} onChange={onInputChange} required />
                                                </div>
                                            </div>
                                        </div>

                                        {/* gender */}
                                        <div className="row">
                                            <div className="col-lg-6">
                                                <label htmlFor="exampleFormControlSelect1">Gender </label>
                                                <div className="form-check">
                                                    <input className="form-check-input" type="radio" name="gender" id="exampleRadios1" checked={editData.gender === "Male"}  value="Male" onChange={onInputChange} />
                                                    <label className="form-check-label" htmlFor="exampleRadios1">
                                                        Male
                                                    </label>
                                                </div>
                                                <div className="form-check">
                                                    <input className="form-check-input" type="radio" name="gender" id="exampleRadios2" checked={editData.gender === "Female"} value="Female" onChange={onInputChange} />
                                                    <label className="form-check-label" htmlFor="exampleRadios2">
                                                        Female
                                                    </label>
                                                </div>
                                            </div>

                                            {/* Institution */}
                                            <div className="col-lg-6">
                                                <div className="form-group">
                                                    <label htmlFor="exampleFormControlSelect1">Institution </label>
                                                    <select className="form-control" id="exampleFormControlSelect1" name="ins_id" onChange={onInputChange}>
                                                        <option>1</option>
                                                        <option>2</option>
                                                        <option>3</option>
                                                        <option>4</option>
                                                        <option>5</option>
                                                    </select>
                                                </div>
                                            </div>

                                            {/* dob */}
                                            <div className="col-lg-6">
                                                <div className="form-group">
                                                    <label className="form-control-label" htmlFor="date">Date</label>
                                                    <input type="date" id="date" className="form-control" name='dob' defaultValue={editData.dob} onChange={onInputChange} />
                                                </div>
                                            </div>

                                            {/* nid */}
                                            <div className="col-lg-6">
                                                <div className="form-group">
                                                    <label className="form-control-label" htmlFor="nid">NID</label>
                                                    <input type="number" id="nid" className="form-control" name="nid" defaultValue={editData.nid} onChange={onInputChange} />
                                                </div>
                                            </div>

                                            {/* phone */}
                                            <div className="col-lg-6">
                                                <div className="form-group">
                                                    <label className="form-control-label" htmlFor="phone">Phone</label>
                                                    <input type="number" id="phone" className="form-control" name="phone" defaultValue={editData.phone} onChange={onInputChange} />
                                                </div>
                                            </div>

                                            {/* altemail */}
                                            <div className="col-lg-6">
                                                <div className="form-group">
                                                    <label className="form-control-label" htmlFor="alt_email">Alt Email address</label>
                                                    <input type="email" id="alt_email" name='alt_email' className="form-control" defaultValue={editData.alt_email} onChange={onInputChange} />
                                                </div>
                                            </div>

                                            {/* address */}
                                            <div className="col-lg-12">
                                                <div className="form-group">
                                                    <label className="form-control-label" htmlFor="address">Address</label>
                                                    <input id="address" name='address' className="form-control" defaultValue={editData.address} type="text" onChange={onInputChange} />
                                                </div>
                                            </div>

                                            {/* pro_pic */}
                                            <div className="col-lg-6">
                                                <div className="form-group">
                                                    <label className="form-control-label" htmlFor="pro_img">Profile Image</label>
                                                    <img src={"http://localhost:8000/" + editData.pro_pic} height={60} width={50} alt=""></img>
                                                    <input type="file" id="pro_img" name='pro_pic' className="form-control" onChange={onInputChangeFile} />
                                                </div>
                                            </div>

                                            {/* cover_pic */}
                                            <div className="col-lg-6">
                                                <div className="form-group">
                                                    <label className="form-control-label" htmlFor="cover_img">Cover Image</label>
                                                    <img src={"http://localhost:8000/" + jsxData.editData} height={60} width={50} alt=""></img>
                                                    <input type="file" id="cover_img" name='cover_pic' className="form-control" onChange={onInputChangeFile} />
                                                </div>
                                            </div>
                                        </div>

                                        <center>
                                            <div>
                                                <button className='btn-success'>Submit</button>
                                                {/* <input type="submit" value="Submit" className='btn-success'/> */}
                                            </div>
                                        </center>

                                    </div>

                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    )
}

const mapStateToProps = (state) => {
    return {
        values: state.profile.profile
    }
}

export default connect(mapStateToProps, { EDIT_USER_PROFILE, UPDATE_USER_PROFILE })(EditProfile)
