import React, { useEffect } from "react";
import { connect } from "react-redux";
import { BECOME_TEACHER } from "../../redux/actions/become_teacher/become_teacherAction";
import { GET_ALL_USER } from "../../redux/actions/get_allUserAction/getAllUserAction";
import { Link } from "react-router-dom";
import Sidebar from "../../utils/Layouts/Partial/Sidebar";
import Search from "../../utils/Layouts/Partial/Search";
import RightSideMenu from "../../utils/Layouts/Partial/RightSideMenu";
import RightSideImageName from "../../utils/Layouts/Partial/RightSideImageName";
import BreadCrumb from "../../utils/Layouts/Partial/BreadCrumb";
import BreadCrumbRightSide from "../../utils/Layouts/Partial/BreadCrumbRightSide";
import TopNavbar from "../../utils/Layouts/Partial/TopNavbar";

const Users = props => {
    let jsxData = [];
    const email = localStorage.getItem("user-email");

    const BecomeTeacher = () => {
        props.BECOME_TEACHER({ email: email });
    };

    useEffect(() => {
        props.GET_ALL_USER();

    }, []);

    const jsxFunc = () => {
        jsxData = [];
        if(props.values.values){
          props.values.values.map((item, valkey) => {
            jsxData.push(
                <tr key={valkey}>
                    <td className="budget">{valkey + 1}</td>
                    <td className="budget">{item.name}</td>
                    <td className="budget">{item.email}</td>
                    <td className="budget">{item.phone}</td>

                    <td>
                        <span className="badge badge-dot mr-4">
                            <i className="bg-warning"></i>
                            <span className="status">pending</span>
                        </span>
                    </td>

                    <td>{item.created_at}</td>
                    <td>
                    <Link to = {`/profile/${item.id}`}><button className='btn btn-success btn-sm'> view  </button></Link>
                    </td>
                </tr>
            );
        });
       }
    };

    // const viewUser = (id) =>{

    // }


    const openModal = () => {
        alert("modal open");
    };

    return (
        <div onLoad={jsxFunc()}>
            <BreadCrumb title={"Users"} openModal={openModal} />


            <div className="container-fluid mt--6">
                <div className="row text-center">
                    <div className="col">
                        <div className="card">
                            <div className="card-header border-0">
                                <h3 className="mb-0">Users</h3>
                            </div>
                            
                            <div className="table-responsive">
                            <table className="table align-items-center table-flush">
                                <thead className="thead-light">
                                    <tr>
                                        <th
                                            scope="col"
                                            className="sort"
                                            data-sort="name"
                                        >
                                            SL
                                        </th>
                                        <th
                                            scope="col"
                                            className="sort"
                                            data-sort="budget"
                                        >
                                            Name
                                        </th>
                                        <th
                                            scope="col"
                                            className="sort"
                                            data-sort="status"
                                        >
                                            Email
                                        </th>
                                        <th scope="col">Phone</th>
                                        <th
                                            scope="col"
                                            className="sort"
                                            data-sort="completion"
                                        >
                                            Status
                                        </th>
                                        <th scope="col">Created Date</th>
                                        <th
                                            scope="col"
                                            className="sort"
                                            data-sort="status"
                                        >
                                             View
                                        </th>
                                    </tr>
                                </thead>
                                <tbody className="list">

                                    {jsxData}
                                </tbody>
                            </table></div>
                        </div>
                    </div>
                </div>
            </div>
            {/* <button onClick={AllUser()}> click user</button> */}
        </div>
    );
};

const mapStateToProps = state => {
    return {
        values: state.users.getAllUser
    };
};

export default connect(mapStateToProps, { GET_ALL_USER })(Users);
