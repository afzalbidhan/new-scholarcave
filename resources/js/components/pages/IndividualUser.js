import React,{useEffect} from 'react'
import BreadCrumb from '../../utils/Layouts/Partial/BreadCrumb'
import { connect } from "react-redux";
import { SHOW_INDIVIDUAL_USER } from "../../redux/actions/get_allUserAction/getIndividualUser";
import {useParams} from 'react-router-dom';

const IndividualUser = (props) => {

    const {id}  = useParams();

    //console.log(id);

    useEffect(() => {
        props.SHOW_INDIVIDUAL_USER(id)
    }, [])

    let jsxData = {}
    const jsxFunc = () => {
        if (props.values.values) {
            jsxData =props.values.values
        }
    }

    return (
        <div  onLoad={jsxFunc()}>

            <BreadCrumb title={"Individual User"}  />

            {/* style="min-height: 500px; background-image: url(../assets/img/theme/profile-cover.jpg); background-size: cover; background-position: center top;" */}
            {/* <div className="header pb-6 d-flex align-items-center" style={{minHeight: `500px`,backgroundImage: 'url("argon/assets/img/theme/profile-cover.jpg")'}}>

                <span className="mask bg-gradient-default opacity-8"></span>

                <div className="container-fluid d-flex align-items-center">
                    <div className="row">
                    <div className="col-lg-7 col-md-10">
                        <h1 className="display-2 text-white">Hello Jesse</h1>
                        <p className="text-white mt-0 mb-5">This is your profile page. You can see the progress you've made with your work and manage your projects or assigned tasks</p>
                        <a href="#!" className="btn btn-neutral">Edit profile</a>
                    </div>
                    </div>
                </div>
            </div> */}

            <div className="container-fluid mt--8">
                <div className="row">
                <center>
                        <div className="col-xl-6 order-xl-6">
                            <div className="card card-profile">
                                <img src="../argon/assets/img/theme/img-1-1000x600.jpg" 
                                alt="Image placeholder" className="card-img-top" />
                                <div className="row justify-content-center">
                                <div className="col-lg-3 order-lg-2">
                                    <div className="card-profile-image">
                                        <a href="#">
                                            <img src="../argon/assets/img/theme/team-4.jpg" className="rounded-circle" />
                                        </a>
                                    </div>
                                </div>
                                </div>
                                <div className="card-header text-center border-0 pt-8 pt-md-4 pb-0 pb-md-4">
                                <div className="d-flex justify-content-between">
                                    <a href="#" className="btn btn-sm btn-info  mr-4 ">Connect</a>
                                    <a href="#" className="btn btn-sm btn-default float-right">Message</a>
                                </div>
                                </div>
                                <div className="card-body pt-0">
                                <div className="row">
                                    <div className="col">
                                    <div className="card-profile-stats d-flex justify-content-center">
                                        <div>
                                        <span className="heading">22</span>
                                        <span className="description">Friends</span>
                                        </div>
                                        <div>
                                        <span className="heading">10</span>
                                        <span className="description">Photos</span>
                                        </div>
                                        <div>
                                        <span className="heading">89</span>
                                        <span className="description">Comments</span>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                <div className="text-center">
                                    <h5 className="h3">
                                    {jsxData.name}<span className="font-weight-light"></span>
                                    </h5>
                                    <h5 className="h3">
                                    {jsxData.email}<span className="font-weight-light"></span>
                                    </h5>
                                    <div className="h5 font-weight-300">
                                    <i className="ni location_pin mr-2"></i>Bucharest, Romania
                                    </div>
                                    <div className="h5 mt-4">
                                    <i className="ni business_briefcase-24 mr-2"></i>Solution Manager - Creative Tim Officer
                                    </div>
                                    <div>
                                    <i className="ni education_hat mr-2"></i>University of Computer Science
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>

                    </center>
                </div>
            </div>

        </div>
    )
}

const mapStateToProps = (state) =>{
    return {
        values :state.users.showIndividualUser
    }
}

export default connect(mapStateToProps , {SHOW_INDIVIDUAL_USER})(IndividualUser)
