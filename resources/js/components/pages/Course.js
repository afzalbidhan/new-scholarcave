import React, { useState, useEffect } from 'react'
import { connect } from "react-redux";
import { ADD_COURSE, GET_ALL_COURSE,DELETE_COURSE , UPDATE_COURSE, EDIT_COURSE,GET_EDIT_COURSE} from "../../redux/actions/courseAction/courseAction";
import BreadCrumb from "../../utils/Layouts/Partial/BreadCrumb";
import Modal from 'react-modal'
import { Link } from "react-router-dom";


const Course = (props) => {
    Modal.setAppElement('#app')

    const [timesEdited,setTimesEdited] = useState(0)

    ////////////////
    // Modal Start

    const customStyles = {
        content: {
            top: '50%',
            left: '50%',
            right: 'auto',
            bottom: 'auto',
            marginRight: '-50%',
            transform: 'translate(-50%, -50%)',
        },
    };

    const [modelOpenStatus, setmodelOpenStatus] = useState(false)

    const [editModelOpenStatus,setEditModelOpenStatus] = useState(false)
   
    const [courseInfo, setCourseInfo] = useState({
        institute_id: null,
        course_code: null,
        course_name: null,
        user_id: localStorage.getItem('user-id'),
    })

    const onInputChange = (e) => {
        let course = { ...courseInfo, [e.target.name]: e.target.value }
        setCourseInfo(course);
    }



    useEffect(() => {
       
        takeUserId();
        props.GET_ALL_COURSE();
      
    },[timesEdited]);


    const takeUserId = async () =>{
        let userId = await localStorage.getItem('user-id');
        await setCourseInfo({...courseInfo,userId}) ;
    }


    const onsubmit = (e) => {
        e.preventDefault();
        if(courseInfo.course_code != null && courseInfo.course_name != null )
        {

            props.ADD_COURSE(courseInfo)

           // props.GET_ALL_COURSE();

            // if(value ==true)
            // {
            setTimesEdited(timesEdited+1)

            setCourseInfo({institute_id: null,
                course_code: null,
                course_name: null,
                user_id: localStorage.getItem('user-id'),})

            setmodelOpenStatus(false)
          //  }
            // else{
            //     alert('Please Add a uniqe code.')
            // }

        }
        else{
            alert('fill in all the data')
        }

    }



    let jsxData = [];

    const jsxFunc = () => {
        jsxData = [];
        if (props.values.values) {
            props.values.values.map((item, valkey) => {
                jsxData.push(
                    <tr key={valkey}>
                        <td className="budget">{valkey + 1}</td>
                        <td className="budget">{item.ins_id}</td>
                        <td className="budget">{item.course_code}</td>
                        <td className="budget">{item.course_name}</td>
                        <td className="budget">{item.created_by}</td>
                        <td className="budget">{item.updated_by}</td>
                        <td>{item.created_at}</td>
                        <td className="budget">{item.course_status == 0 ?

                        <span className="badge badge-dot mr-4">
                            <i className="bg-warning"></i>
                            <span className="status">pending</span>
                        </span>
                        :
                        <span className="badge badge-dot mr-4">
                            <i className="bg-warning"></i>
                            <span className="status">Active</span>
                        </span>}</td>

                        <td>
                            <button className='btn-sm btn-primary mr-2' onClick={()=>{editCourse(item.id)}}>Edit</button>
                            <button className='btn-sm btn-danger' onClick={()=>{deleteCourse(item.id)}}>Delete</button>
                        </td>



                    </tr>
                );
            });
        }

        if(props.values.editValues){
           if(props.values.editValues.edited == 0){
               
                  setEditCourseInfo({
                    institute_id: null,
                    course_id:  props.values.editValues.id,
                    course_code: props.values.editValues.course_code,
                    course_name: props.values.editValues.course_name,
                    user_id: localStorage.getItem('user-id'),
                   
                })

                props.values.editValues.edited = 1;
                console.log( props.values.editValues);


            }
            
        }
    };
  

    const deleteCourse = async(id)=>{
        await(props.DELETE_COURSE(id))
        setTimesEdited(timesEdited+1)
    }


    const [editcourseInfo, setEditCourseInfo] = useState({
        institute_id: '',
        course_id:'',
        course_code: '',
        course_name: '',
        user_id: localStorage.getItem('user-id'),
      
    })

    let editData =[]

   

    const editCourse = async(id)=>{
   

     let result = await props.GET_EDIT_COURSE(id);
    
     
     await setEditModelOpenStatus(true)

    

    }



    const onInputEditChange = (e) => {
        let course = { ...editcourseInfo, [e.target.name]: e.target.value }
        setEditCourseInfo(course);
    }

    const onEditsubmit = (e) =>{
        e.preventDefault();
        if(editcourseInfo.course_code != "" && editcourseInfo.course_name != "" )
        {

            /* let value=props.UPDATE_COURSE(editcourseInfo)
            alert(value)
            if(value==true)
            {setTimesEdited(timesEdited+1)

            setEditCourseInfo({
                institute_id: "",
                course_id:"",
                course_code: "",
                course_name: "",
                user_id: localStorage.getItem('user-id'),
            })

                setEditModelOpenStatus(false)
            }
            else{
                alert('Please change unique code')
            } */

            let value=props.UPDATE_COURSE(editcourseInfo)
            setTimesEdited(timesEdited+1)
            setEditCourseInfo({
                institute_id: "",
                course_id:"",
                course_code: "",
                course_name: "",
                user_id: localStorage.getItem('user-id'),
            })
            setEditModelOpenStatus(false)

        }
        else{
            alert('fill in all the data')
        }
    }

    // console.log(editcourseInfo)
    // edit end
    //////////////////




    //////////////////////
    // modal func start

    const openModal =() =>{
        setmodelOpenStatus(true)
    }

    // modal func start
    //////////////////






    return (
        <div onLoad={  jsxFunc()} id="mainDiv">
           
            <BreadCrumb title={"Course"} openModal={openModal}/>
            <br />
            <br />


            {/* Modal started */}
            <Modal isOpen={modelOpenStatus} style={customStyles} >

   
                <h1>Add Course</h1>
                <br />
                <br />
                <form onSubmit={onsubmit}>
                    {/* <h6 className="heading-small text-muted mb-4">Add Course</h6> */}
                    <div className="">
                        <div className="row text-center">

                            {/* Instituion Id */}
                            <div className="col-lg-12">
                                <div className="form-group">
                                    <label htmlFor="exampleFormControlSelect1">Institution </label>
                                    <select className="form-control" id="institute_id" name="institute_id" >
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                    </select>
                                </div>
                            </div>

                            {/* Course Code */}
                            <div className="col-lg-12">
                                <div className="form-group">
                                    <label className="form-control-label" htmlFor="email">Course Code</label>
                                    <input type="text" id="course_code" name="course_code" className="form-control" onChange={onInputChange} />
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="">
                        <div className="row text-center">

                            {/* Course Name */}
                            <div className="col-lg-12">
                                <div className="form-group">
                                    <label className="form-control-label" htmlFor="name">Course Name</label>
                                    <input type="text" id="course_name" name="course_name" className="form-control" onChange={onInputChange} />
                                </div>
                            </div>

                        </div>
                    </div>

                    <div className="text-center">
                        <input type="submit" value="Submit" className='btn btn-success' />
                    </div>
                </form>

                <br />
                <center>
                    <button onClick={() => setmodelOpenStatus(false)} className="btn-danger">Close</button>
                </center>
            </Modal>

            {/* Modal end */}


            {/* Modal started */}
            <Modal isOpen={editModelOpenStatus} style={customStyles}>


                <h1>Edit Course</h1>
                <br />
                <br />
                <form onSubmit={onEditsubmit}>
                    {/* <h6 className="heading-small text-muted mb-4">Add Course</h6> */}
                    <div className="">
                        <div className="row text-center">

                            {/* Instituion Id */}
                            <div className="col-lg-12">
                                <div className="form-group">
                                    <label htmlFor="exampleFormControlSelect1">Institution </label>
                                    <select className="form-control" id="institute_id" name="institute_id" >
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                    </select>
                                </div>
                            </div>

                            {/* Course Code */}
                            <div className="col-lg-12">
                                <div className="form-group">
                                    <label className="form-control-label" htmlFor="email">Course Code</label>
                                    <input type="text" id="course_code" name="course_code" className="form-control"  defaultValue={editcourseInfo.course_code} onChange={onInputEditChange} />
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="">
                        <div className="row text-center">

                            {/* Course Name */}
                            <div className="col-lg-12">
                                <div className="form-group">
                                    <label className="form-control-label" htmlFor="name">Course Name</label>
                                    <input type="text" id="course_name" name="course_name" className="form-control" defaultValue={editcourseInfo.course_name} onChange={onInputEditChange} />
                                </div>
                            </div>


                        </div>
                    </div>

                    <div className="text-center">
                        <input type="submit" value="Submit" className='btn btn-success' />
                    </div>
                </form>

                <br />
                <center>
                    <button onClick={() => setEditModelOpenStatus(false)} className="btn-danger">Close</button>
                </center>
            </Modal>

            {/* Modal ended */}

            <div className="container-fluid mt--6">
                <div className="row text-center">
                    <div className="col">
                        <div className="card">
                            <div className="card-header border-0">
                                <h3 className="mb-0">Course</h3>
                            </div>
                            
                            <div className="table-responsive">
                            <table className="table align-items-center table-flush">
                                <thead className="thead-light">
                                    <tr>
                                        <th
                                            scope="col"
                                            className="sort"
                                            data-sort="name"
                                        >
                                            SL
                                        </th>
                                        <th
                                            scope="col"
                                            className="sort"
                                            data-sort="budget"
                                        >
                                            Ins Id
                                        </th>
                                        <th
                                            scope="col"
                                            className="sort"
                                            data-sort="status"
                                        >
                                            Course Code
                                        </th>
                                        <th
                                            scope="col"
                                            className="sort"
                                            data-sort="completion"
                                        >
                                            Course Name
                                        </th>
                                        <th
                                            scope="col"
                                            className="sort"
                                            data-sort="status"
                                        >
                                            Created By
                                        </th>
                                        <th
                                            scope="col"
                                            className="sort"
                                            data-sort="completion"
                                        >
                                            Updated By
                                        </th>
                                        <th scope="col">Created Date</th>
                                        <th
                                            scope="col"
                                            className="sort"
                                            data-sort="status"
                                        >
                                            Status
                                        </th>
                                        <th
                                            scope="col"
                                            className="sort"
                                            data-sort="status"
                                        >
                                            Action
                                        </th>
                                    </tr>
                                </thead>
                                <tbody className="list">

                                    {jsxData}
                                </tbody>
                            </table></div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    )
}

const mapStateToProps = (state) => {
    return {
        values: state.course.getAllCourse
    }
}


export default connect(mapStateToProps, 
    { 
    ADD_COURSE,
    GET_ALL_COURSE, 
    DELETE_COURSE, 
    UPDATE_COURSE,
    GET_EDIT_COURSE
   })(Course)
