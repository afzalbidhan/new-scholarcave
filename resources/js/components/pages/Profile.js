import React, { useEffect, useState } from 'react'
import BreadCrumb from '../../utils/Layouts/Partial/BreadCrumb'
import { connect } from "react-redux";
import { CURRENT_USER_PROFILE } from "../../redux/actions/profileAction/profileAction";
import { Link } from "react-router-dom";

const Profile = (props) => {

    const email = localStorage.getItem('user-email');

    // const [editTimes,setEditTimes] = useState(0);

    useEffect(() => {
        props.CURRENT_USER_PROFILE(email)
    }, [])


    // let user

    // let jsFunc = () =>{

    //     if(props.values.values){
    //         user = props.values.values;

    //         return user
    //     }

    // }

    // let test = jsFunc()
    // console.log(test.id)

    // let user = {};
    // user = props.values.values;
    // console.log(user)



    let jsxData = {}
    const jsxFunc = () => {
        if (props.values.values) {
            jsxData = props.values.values
        }
    }

    // jsxFunc()
    // console.log(jsxData)

    return (
        <div onLoad={jsxFunc()}>

            <BreadCrumb title={"Profile"} />

            {/* style="min-height: 500px; background-image: url(../assets/img/theme/profile-cover.jpg); background-size: cover; background-position: center top;" */}
            {/* <div className="header pb-6 d-flex align-items-center" style={{minHeight: `500px`,backgroundImage: 'url("argon/assets/img/theme/profile-cover.jpg")'}}>

                <span className="mask bg-gradient-default opacity-8"></span>

                <div className="container-fluid d-flex align-items-center">
                    <div className="row">
                    <div className="col-lg-7 col-md-10">
                        <h1 className="display-2 text-white">Hello Jesse</h1>
                        <p className="text-white mt-0 mb-5">This is your profile page. You can see the progress you've made with your work and manage your projects or assigned tasks</p>
                        <a href="#!" className="btn btn-neutral">Edit profile</a>
                    </div>
                    </div>
                </div>
            </div> */}

            <div className="container-fluid mt--5">
                <div className="row">

                    <div className="col-md-6">
                        <div className="card card-profile">
                            <img src="argon/assets/img/theme/img-1-1000x600.jpg" alt="Image placeholder" className="card-img-top" />
                            <div className="row justify-content-center">
                                <div className="col-lg-3 order-lg-2">
                                    <div className="card-profile-image">
                                        <a href="#">
                                            <img src="argon/assets/img/theme/team-4.jpg" className="rounded-circle" />
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div className="card-header text-center border-0 pt-8 pt-md-4 pb-0 pb-md-4">
                                <div className="d-flex justify-content-between">
                                    <div>
                                        <Link className="btn btn-sm btn-default float-right" to={`/profile/${jsxData.id}/edit`}>Edit</Link>
                                    </div>
                                </div>
                            </div>
                            <div className="card-body pt-0">

                                <div className="text-center">
                                    <h5 className="h3">
                                        {jsxData.name}<span className="font-weight-light"></span>
                                    </h5>
                                    <h5 className="h3">
                                        {jsxData.email}<span className="font-weight-light"></span>
                                    </h5>
                                    <div className="h5 font-weight-300">
                                        <i className="ni location_pin mr-2"></i>Bucharest, Romania
                                    </div>
                                    <div className="h5 mt-4">
                                        <i className="ni business_briefcase-24 mr-2"></i>Solution Manager - Creative Tim Officer
                                    </div>
                                    <div>
                                        <i className="ni education_hat mr-2"></i>University of Computer Science
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>



                    <div className="col-md-6">
                        <div className="profile_connect">
                            <h1>Connections</h1>
                            <hr />
                            <div className="card-profile-stats d-flex justify-content-center">
                                <div>
                                    <span className="heading">22</span>
                                    <span className="description">Friends</span>
                                </div>
                                <div>
                                    <span className="heading">10</span>
                                    <span className="description">Photos</span>
                                </div>
                                <div>
                                    <span className="heading">89</span>
                                    <span className="description">Comments</span>
                                </div>
                            </div>
                            <button>Connect</button>
                            
                        </div>
                    </div>

                </div>



            </div>

        </div>
    )
}

const mapStateToProps = (state) => {
    return {
        values: state.profile.profile
    }
}

export default connect(mapStateToProps, { CURRENT_USER_PROFILE })(Profile)
