import React, { useEffect } from "react";
import { connect } from "react-redux";
import { GET_ALL_TEACHER } from "../../redux/actions/get_allTeacherAction/getAllTeacherAction";
import BreadCrumb from "../../utils/Layouts/Partial/BreadCrumb";

const Teachers = (props) => {

    let jsxData = [];

    useEffect(() => {
        props.GET_ALL_TEACHER();
    }, []);

    const jsxFunc = () => {
        jsxData = [];
        if(props.values.values){        
          props.values.values.map((item, valkey) => {
            jsxData.push(
                <div className="col-4" key={valkey}>
                    <div className="card" >
                         <img src="argon/assets/img/theme/teacher2.jpg" alt="Image placeholder" className="rounded" />
                        <div className="card-body">
                            <h5 className="card-title">{item.name}</h5>
                            <p className="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                            <p className="card-text">{item.created_at}</p>
                        </div>
                    </div>
                </div>
            );
        });
       }
    };

    
    const openModal = () => {
        alert("modal open");
    };

    
    return (
        <div onLoad={jsxFunc()}>
            <BreadCrumb title={"Teachers"} openModal={openModal} />
            <div className="container-fluid mt--6">
                <div className="row text-center">
                    <div className="col-12">
                        <div className="card">
                            <div className="card-header border-0">
                                <h2 className="mb-0">Teacher List</h2>
                            </div>
                        
                            <div className="row p-3">
                                {jsxData}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

const mapStateToProps = state => {
    return {
        values: state.teacher.getAllTeacher
    };
};

export default connect( mapStateToProps , {GET_ALL_TEACHER}) (Teachers)

