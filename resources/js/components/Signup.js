import React from 'react'
import { signupWithJWT } from '../redux/actions/auth/registerActions'
import { connect } from "react-redux";
import './Signup.css'
import { Link } from 'react-router-dom'
import './Form/Form.css';
import useForm from './Form/useForm';

const Signup = props => {

    const {
        wrongCred,
        correctCred,
        onsubmit,
        handleChange,
        errors
    } = useForm(props);


    return (

        <div className="form_main">
            {props.values.values
                ? props.values.values.error == 401
                    ? wrongCred()
                    : correctCred()
                : null}

            <div className="container">
                <div className="row">
                    <div className="col-md-6 order-1 order-md-2">
                        <div className="overlay-container">
                            <div className="overlay">
                                <div className="overlay-panel overlay-right">
                                    <h1>Hello, Friend</h1>
                                    <p>Enter your personal details and start your journey with us</p>
                                    <Link to="/login">
                                        <button className="ghost" id="signup">
                                            LogIn
                                        </button>
                                    </Link>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="col-md-6 bg order-2 order-md-1">
                        <form onSubmit={onsubmit}>
                            <h1>Sign Up</h1>
                            <div className='form-inputs'>
                                <input
                                    className='form-input'
                                    type='text'
                                    id="name"
                                    name='name'
                                    placeholder='Enter your name'
                                    onChange={handleChange}
                                />
                                {errors.name && <p>*{errors.name}</p>}
                            </div>
                            <div className='form-inputs'>
                                <input
                                    className='form-input'
                                    type='email'
                                    id="email"
                                    name='email'
                                    placeholder='Enter your email'
                                    onChange={handleChange}
                                />
                                {errors.email && <p>*{errors.email}</p>}
                            </div>
                            <div className='form-inputs'>
                                <input
                                    className='form-input'
                                    type='password'
                                    name='password'
                                    id="password"
                                    placeholder='Enter your password'
                                    onChange={handleChange}
                                />
                                {errors.password && <p>*{errors.password}</p>}
                            </div>
                            <div className='form-inputs'>
                                <input
                                    className='form-input'
                                    id="c_password"
                                    type='password'
                                    name='c_password'
                                    placeholder='Confirm your password'
                                    onChange={handleChange}
                                />
                                {errors.c_password && <p>*{errors.c_password}</p>}
                            </div>
                            <button className='form-input-btn' type='submit'>
                                Sign up
                            </button>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    )
}

const mapStateToProps = state => {
    return {
        values: state.auth.login
    };
};
export default connect(mapStateToProps, { signupWithJWT })(Signup);

