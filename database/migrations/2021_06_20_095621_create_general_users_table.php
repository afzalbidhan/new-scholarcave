<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGeneralUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('general_users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');

            $table->string('name');
            $table->string('gender')->nullable();

            $table->integer('ins_id')->unsigned()->nullable();
            $table->foreign('ins_id')->references('id')->on('institutes');

            $table->date('dob')->nullable();
            $table->string('nid')->nullable();
            $table->string('phone')->nullable();
            $table->string('pro_pic')->nullable();
            $table->string('cover_pic')->nullable();
            $table->string('alt_email')->nullable();
            $table->string('address')->nullable();
            $table->boolean('soft_delete')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('general_users');
    }
}
